A collection of possibly useful information for creating the user study:

* http://space.wccnet.edu/~pmillis/cps120/cps120_pgm_syntax.pdf
* http://www.cprogramming.com/tutorial/compiler_linker_errors.html
* http://web.mst.edu/~cpp/common/common_errors.html


----

some links of concrete errors:

* https://stackoverflow.com/questions/26795763/how-to-resolve-the-compilation-error-here-for-the-observer-pattern-in-c
* https://stackoverflow.com/questions/41222330/template-compilation-error-x-does-not-refer-to-a-value
* https://stackoverflow.com/questions/7021624/error-c2678-binary-no-operator-found-which-takes-a-left-hand-operand-of-t
* https://stackoverflow.com/questions/9702859/c-strange-missing-before-error-due-to-a-typedefed-return-type-in-templa
* http://www.cplusplus.com/forum/general/106570/
* http://stackoverflow.com/questions/11888622/c-confusing-compilation-errors


