#include "Vector.h"
#include<string>
using namespace std;

void f2(Vector<string> & vs)
{
    for (auto& s: vs)                    //too few arguments to function call, single argument 'a' was not specified
        cout << s << '\n';
}
