template<typename T>
class Vector{
    public:
        const T* begin(Vector<T>& a);
        const T* end(Vector<T>& b);
};