    #include <iostream>
    #include <map>

    namespace MyNamespace {
      class MyClass;

      typedef bool (*fctPtr)(void);
      typedef std::map<std::string, fctPtr> fctMap;
    };

    class MyNamespace::MyClass {

      public:
        static bool func1(void) { return true; };
        static bool func2(void) { return true; };
        static bool func3(void) { return true; };
        static bool func4(void) { return true; };

    };

    MyNamespace::fctMap MyFctMap;


    void execFct() {
      MyNamespace::MyClass obj;
      MyNamespace::fctPtr fctMemb;

      fctMemb = MyFctMap["func1"];
      (obj.*fctMemb)();
    }


    int main() {
      MyFctMap["func1"] = &MyNamespace::MyClass::func1;
      MyFctMap["func2"] = &MyNamespace::MyClass::func2;
      MyFctMap["func3"] = &MyNamespace::MyClass::func3;
      MyFctMap["func4"] = &MyNamespace::MyClass::func4;

      execFct();
    }
