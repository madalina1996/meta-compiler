#include "DVD.h"
#include "Game.h"
#include "Book.h"

#ifndef LIBRARY_H_
#define LIBRARY_H_

class Library{
public:
    int numGames;
    int numDVDs;
    int numBooks;
    Game collection[100];
    DVD  collection1[100];
    Book collection2[100];

    Library(){
        numGames = 0;
        numDVDs = 0;
        numBooks = 0;
    }

    void insertGame( char gameName[], char platform[], int c);
    void deleteGame( char gameName[]);
    Game *search( char gameName[]);

    void insertDVD( char dvdName[], char director[], int c);
    void deleteDVD( char dvdName[]);
    DVD *search( char dvdName[]);

    void insertBook( char bookName[], char director[], int c);
    void deleteBook( char bookName[]);
    Book *search( char bookName[]);
};

#endif // end of "#ifndef" block
