class time
{
private:
    int _hours;
    int _minutes;
    float _seconds;
    bool checkHours(int hours);
    bool checkMinutes(int minutes);
    bool checkSeconds(float seconds);

public:

time(int hours=0, int minutes=0, float seconds=0);
time(const time & tm);
~time();


void hours(int hours);
int  hours() const;
void minutes(int minutes);
int  minutes() const;
void seconds(float seconds);
float  seconds() const;


void operator=(time tm);
bool operator==(time tm);


void print();
time getTimeFromUser();
float getTimeAsFractionOfTheDay(time tm);

};