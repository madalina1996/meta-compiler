#include <iostream>
#include "time.h"
using namespace std;

bool time::checkHours(int hours)
{
    return hours>=0 && hours<24;
}
bool time::checkMinutes(int MS)
{
    return MS>=0 && MS<60;
}
bool time::checkSeconds(float MS)
{
    return MS>=0 && MS<60;
}

//constractors
time::time(int hours, int minutes, float seconds)
{
    if(checkHours(hours) && checkMinutes(minutes) && checkSeconds(seconds))
    {
        _hours=hours;
        _minutes=minutes;
        _seconds=seconds;
    }
    else
    {
        cout<<"Error"<<endl; _hours=-1; _minutes=-1; _seconds=-1;
    }
}
time::time(const time & tm)
{
    _seconds = tm.seconds();
    _hours = tm.hours();
    _minutes=tm.minutes();
}
time::~time()
{
}

//get-set functions
void time::hours(int hours)
{
    _hours=hours;
}
int  time::hours() const
{
    return _hours;
}
void time::minutes(int minutes)
{
    _minutes=minutes;
}
int  time::minutes() const
{
    return _minutes;
}
void time::seconds(float seconds)
{
    _seconds = seconds;
}

float  time::seconds() const
{
    return _seconds;
}

//operators
void time::operator=(time tm)
{

    _hours=tm.hours();
    _minutes=tm.minutes();
    _seconds=tm.seconds();

}
bool time::operator==(time tm)
{
    return _hours=tm.hours() && _minutes==tm.minutes() && _seconds==tm.seconds();
}

//some function
void time::print()
{
    cout<<" "<<_hours<<":"<<_minutes<<":"<<_seconds<<" "<<endl;
}

time time::getTimeFromUser()
{
    time newTime;
    int userhours=-1;
    int userminutes=-1;
    float userseconds=-1;
    while (!checkHours(userhours))
    {
        cout<<"enter hours"<<endl;
        cin>>userhours;
        if(!checkHours(userhours))
        {
            cout<<"Error try again"<<endl;
        }
    }

    while (!checkMinutes(userminutes))
    {
        cout<<"enter minutes"<<endl;
        cin>>userminutes;
        if(!checkMinutes(userminutes))
        {
            cout<<"Error try again"<<endl;
        }
    }

    while (!checkSeconds(userseconds))
    {
        cout<<"enter Seconds"<<endl;
        cin>>userseconds;
        if(!checkSeconds(userseconds))
        {
            cout<<"Error try again"<<endl;
        }
    }

    newTime.seconds(userseconds);
    newTime.hours(userhours);
    newTime.minutes(userminutes);

    return newTime;
}


float time::getTimeAsFractionOfTheDay(time tm)
{

    return 0.0;
}
