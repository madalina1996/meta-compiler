template <typename T>
struct A
{
    using result_type = T;

    T operator()(T a, T b)
    {
        return a+b;   
    }
};

template <typename Functor>
struct B
{
    using T = typename Functor::result_type;

    T operator()(Functor &&f, T... args)
    {
        return f(args...);
    }
};