#include <stdio.h>

int GetInt() { return 0; }

int main(void)
{
   printf("How tall do you want your pyramid to be?\n");
   int height = GetInt();

   if (height > 23 && height < 1)
   {
       printf("Please use a positive number no greater than 23:\n");
   }
   else (height > 0 && height <= 23)
   {
       printf("Thanks!\n");
   }
}
