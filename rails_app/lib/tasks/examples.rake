require 'open3'


# File produced by Chengnian has examples that look like the one below. 
#
# ===Example===
# id=1
# name=err_expected_semi_decl_list
# ===Section===
# expected ';' at end of declaration list 
# ===Section===
# ---good---
# struct {
#   int s;
#  };
#
# ===Section===
# ---bad---
# struct {
#   int s 1; 
# };


namespace :examples do
	desc 'Import the examples genereated by Chengnian'
	task :import => :environment do
		if ENV['IMPORT_FNAME'].nil?
			puts "USAGE: rails examples:import IMPORT_FNAME=FILENAME"		
			exit 1
		end
		# User id 1 should be admin
		u = User.find(1)
		if u.nil?
			puts 'ERROR: Admin user was not found.'
			exit 1
		end
		# We should have at least one compiler
		c = Compiler.find_by(name: 'clang++-3.9')
		if c.nil?
			puts 'ERROR: clang++-3.9 compiler was not found.'
			exit 1
		end

		contents = nil
		File.open(ENV['IMPORT_FNAME']) { |f| contents = f.read }
		count = 0
		duplicates, dms = 0, {}
		examples = contents.split("===Example===")
		examples.each do |ex|
			if ex.blank?
				puts "ex is blank?"
				next
			end
			count = count + 1
			ex_arr = ex.split("===Section===")	
			name = ex_arr[0].split()[1].split("=")[1]	
			message = ex_arr[1].strip
			dm = DiagnosticMessage.find_or_create_by(compiler_id: c.id, name: name, message: message)
			if dm.created_by.nil?
				dm.created_by = u.id
				dm.save
			end
			#puts dm.inspect
			#exit
			example = Example.new	
			example.diagnostic_message_id = dm.id
			fixed = ex_arr[2].gsub("---good---","").strip
			example.fixed = fixed
			buggy = ex_arr[3].gsub("---bad---","").strip
			example.buggy = buggy
			example.created_by = u.id
			#puts example.inspect
			example.tokenize_and_hash
			if !example.save
				puts "ERROR\t#{count}"
				puts 'example.errors.inspect'
				puts example.errors.inspect
				duplicates = duplicates + 1
				if dms.has_key?(dm.message)
					dms[dm.message] = dms[dm.message] + 1 
				else
					dms[dm.message] = 1
				end
			else
				puts "SUCESSS\t#{count}"
			end
		end # examples.each
		puts "DUPLICATES\t#{duplicates}"
		$stderr.puts dms.inspect
	end #task


	desc 'Compile buggy and fixed code'
	task :compile => :environment do
		#Example.all.in_groups_of(1000,false).each_with_index do |group,i|
			Parallel.each(Example.unscoped.all.order(:id), in_processes: 8) do |e|
				#Example.transaction do
					puts "Example #{e.id}"
					#group.each do |e|
						begin
							e.compile 
						rescue Exception => err
							puts "ERROR: Example #{e.id}"
						exit
						end # begin
					#end # each
				#end # transaction
			end # parallel
			Example.connection.reconnect!
		#end # groups 
	end # task


	desc 'Find cases where the current diagnostic message does not match the extracted diagnostic message.'
	task :dm_anomalies => :environment do
		Example.unscoped.all.order(:id).each do |e|
			dm_extracted = e.extract_diagnostic_message
			dm_assigned = e.diagnostic_message.message
			if dm_extracted == dm_assigned
				#puts "N\tExample:#{e.id}"
				#print '.'
			else
				e.compile # recompile just to be sure
				if dm_extracted == dm_assigned
					#puts "N\tExample:#{e.id}"
				else
					puts "Y\tExample: #{e.id}\tDM ID: #{e.diagnostic_message.id}\tCurrent: #{dm_assigned.inspect}\tExtracted: #{dm_extracted.inspect}"
					updated = e.update_diagnostic_message_id
					if !updated 
						puts "ERROR: Failed to update diagnostic_message_id."
						puts e.errors.inspect
						if e.errors.messages.has_key?(:fix_uniqueness)
							puts "Duplicate\t#{e.id}"
							puts "Deleting\t#{e.id}"
							#e.destroy
						else
							puts errors.inspect
						end
					else 
						puts "SUCCESS: Updated diagnostic_message_id."
					end
					sleep(5)		
				end
			end
		end
	end


	desc 'Remove examples where the buggy code is not buggy'
	task :clean_non_buggy => :environment do
		es = Example.buggy_not_buggy
		Example.transaction do 
			es.each do |e|
				e.compile # force refresh
			end
		end
		es = Example.buggy_not_buggy
		#es.each do |e|
		#	e.buggy.encode('UTF-8',invalid: :replace, undef: :replace)
		#	e.fixed.encode('UTF-8',invalid: :replace, undef: :replace)
		#end
		t = Time.now.strftime('%Y-%m-%d_%H-%M-%S')
		File.open("clean_result_#{t}",'w') do |f|
			f.write(es.to_s.encode('UTF-8').to_json)
		end
	end


	desc 'Remove examples where the buggy code is not buggy'
	task :clean_duplicates => :environment do
		es = Example.where(buggy_compilation_output:nil)
		Example.transaction do 
			es.each do |e|
				e.compile # force refresh
			end
		end
		es = Example.where(buggy_compilation_output:nil)
		es.each_with_index do |e,i|
			puts "#{i}\tDeleting\t#{e.id}"
			e.destroy
		end
		#t = Time.now.strftime('%Y-%m-%d_%H-%M-%S')
		#File.open("clean_result_#{t}",'w') do |f|
		#	f.write(es.to_s.encode('UTF-8').to_json)
		#end
	end


	desc 'Broken fixed code'
	task :broken => :environment do
		Example.unscoped.find_each do |e|
			puts "Example\t#{e.id}"
			dm = e.extract_diagnostic_message(e.compiler_output_fixed)
			if dm.blank?
			else
				puts 'Broken'
				puts e.inspect
				exit
			end
		end
	end	


	desc 'Sample some examples'
	task :sample => :environment do
		count = 0
		puts "count, sha1, diagnostic_message"
		Example.unscoped.order("RANDOM()").each do |e|
			count = count + 1
			sha1 = Digest::SHA1.hexdigest(e.diagnostic_message.message)
			puts "#{count}, #{sha1}, #{e.extract_diagnostic_message}"
		end
	end


	desc 'Tokenize the examples generated by Chengnian'
	task :tokenize => :environment do
		count = 0
		Example.all.each do |e|
			count = count + 1
			puts "Tokenizing \t#{count}"
			e.tokenize_buggy_and_fixed
		end # each
	end # task


	desc 'Hash the token sequence of the examples'
	task :hash_tokens => :environment do
		count = 0
		Example.all.each do |e|
			count = count + 1
			puts "Hashing \t#{count}"
			e.hash_buggy_and_fixed_tokens
		end # each
	end # task


	desc 'Tokenize the examples and Hash the token sequence'
	task :tokenize_and_hash => :environment do
		batch = 1
		Example.all.in_groups_of(1000,false) do |group|
				Example.transaction do
					puts "BATCH #{batch}"
					batch += 1
					group.each do |e|
						begin
							e.tokenize_and_hash
							e.save
						rescue Exception => err
							puts "ERROR: Example #{e.id}"
							exit
						end # begin
					end # each
				end # transaction
		end # in groups of 
	end # task


	desc 'Format buggy and fixed code.'
	task :format => :environment do
		count = 0
		Example.all.each do |e|
			count = count + 1
			puts "Formatting\t#{count}"
			e.format_and_save
		end # each
	end # task


	desc 'Format, tokenize, and diff code.'
	task :format_tokenize_diff => :environment do
		count = 0
		Example.all.each do |e|
			count = count + 1
			puts "FT&Ding\t#{count}"
			e.format
			e.tokenize_and_hash	
			e.diff_tokens
			e.save
		end # each
	end # task

	desc 'Update Patch'
	task :update_patch => :environment do
		#Example.all.in_groups_of(1000,false).each_with_index do |group,i|
			Parallel.each(Example.unscoped.all.order(:id), in_threads: 6) do |e|
				#Example.transaction do
					puts "Example #{e.id}"
					#group.each do |e|
						begin
							e.set_patch
						rescue Exception => err
							puts "ERROR: Example #{e.id}"
						exit
						end # begin
					#end # each
				#end # transaction
			end # parallel
			Example.connection.reconnect!
		#end # groups 
	end # task


	desc 'Update Patch where change is replace'
	task :update_patch_replace => :environment do
		#Example.all.in_groups_of(1000,false).each_with_index do |group,i|
			Parallel.each(Example.same_token_size, in_processes: 6) do |e|
				#Example.transaction do
					puts "Example #{e.id}"
					#group.each do |e|
						begin
							e.set_patch
						rescue Exception => err
							puts "ERROR: Example #{e.id}"
						exit
						end # begin
					#end # each
				#end # transaction
			end # parallel
			Example.connection.reconnect!
		#end # groups 
	end # task

	
	desc 'Update sizes'
	task :update_sizes => :environment do
		#Example.all.in_groups_of(1000,false).each_with_index do |group,i|
			Parallel.each(Example.all, in_processes: 6) do |e|
				#Example.transaction do
					puts "Example #{e.id}"
					#group.each do |e|
						begin
							e.set_sizes
						rescue Exception => err
							puts "ERROR: Example #{e.id}"
						exit
						end # begin
					#end # each
				#end # transaction
			end # parallel
			Example.connection.reconnect!
		#end # groups 
	end # task

	desc 'Dump fix example sizes'
	task :dump_sizes => :environment do
		puts 'id,buggyLoc,fixedLoc,buggyTokens,fixedTokens'
		Example.unscoped.order(:id).each do |e|
			puts "#{e.id},#{e.buggy_loc},#{e.fixed_loc},#{e.buggy_tokens_size},#{e.fixed_tokens_size}" 
		end	
	end

end
