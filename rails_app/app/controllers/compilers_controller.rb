class CompilersController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_compiler, only: [:show, :edit, :update, :destroy]

  # GET /compilers
  # GET /compilers.json
  def index
    @compilers = Compiler.all
  end

  # GET /compilers/1
  # GET /compilers/1.json
  def show
		@templates = Template.where(compiler_id: @compiler.id).paginate(:page => params[:page])
  end

  # GET /compilers/new
  def new
		Rails.logger.debug 'CompilersController.new'
    @compiler = Compiler.new
  end

  # GET /compilers/1/edit
  def edit
  end

  # POST /compilers
  # POST /compilers.json
  def create
		Rails.logger.debug 'CompilersController.create'
    @compiler = Compiler.new(compiler_params)

    respond_to do |format|
      if @compiler.save
        format.html { redirect_to @compiler, notice: 'Compiler was successfully created.' }
        format.json { render :show, status: :created, location: @compiler }
      else
        format.html { render :new }
        format.json { render json: @compiler.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /compilers/1
  # PATCH/PUT /compilers/1.json
  def update
    respond_to do |format|
      if @compiler.update(compiler_params)
        format.html { redirect_to @compiler, notice: 'Compiler was successfully updated.' }
        format.json { render :show, status: :ok, location: @compiler }
      else
        format.html { render :edit }
        format.json { render json: @compiler.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /compilers/1
  # DELETE /compilers/1.json
  def destroy
		#TODO: Only admin can delete
		render nothing: true, status: :unauthorized
    #@compiler.destroy
    #respond_to do |format|
    #  format.html { redirect_to compilers_url, notice: 'Compiler was successfully destroyed.' }
    #  format.json { head :no_content }
    #end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_compiler
      @compiler = Compiler.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def compiler_params
      params.require(:compiler).permit(:family, :name, :description, :url, :created_by)
    end
end
