class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  rescue_from ActionController::RedirectBackError, with: :redirect_to_default
	after_filter :allow_iframe

	#http://stackoverflow.com/questions/16561066/ruby-on-rails-4-app-does-not-work-in-iframe
	def allow_iframe
    response.headers.delete "X-Frame-Options"
	end

	def redirect_to_default
		redirect_to root_url
	end

	def require_admin
		unless current_user.admin?
			redirect_to root_path, notice: 'Not authorized'
		end
	end

end
