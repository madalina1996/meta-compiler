require 'levenshtein'

class WelcomeController < ApplicationController

	# GET /  
  def index
		@compilers = Compiler.all	
		@dms = DiagnosticMessage.first(5)
		@examples = Example.unscoped.order("RANDOM()").first(10)
		if !@examples.empty?
			example = @examples.first
			@sample_code = "//Example #{example.id}\n" 
			@sample_code += example.buggy
		end
  end
	

  # POST /fix_search
  # POST /fix_search.json
	def compiler 
		@compiler_id = 1 # only clang for now
		@code = params[:buggy]
	end # fix_search


	# POST /compile.json
	def compile
		@compiler_id = 1 # only clang for now
		@cmd = "clang++-3.9 -c -Wfatal-errors -stdlib=libc++ -std=c++14"
		@code = params[:code]
		compilation = @code.compile('c++')
		@compilation_output, @compilation_status = compilation[0], compilation[1]
		@message = @compilation_output.extract_diagnostic_message('c++') # extract dm

		# Record Event
		event = CompilationEvent.new
		if user_signed_in? 
			event.user_id = current_user.id 
		end
		event.code = @code
		event.command = @cmd
		event.output = @compilation_output
		event.error = @message
		event.examples_count = nil
		event.fixes_count = nil 
		if !@message.blank?
			event.event = 'error'
		elsif @message.blank? && !@compilation_output.blank?
			event.event = 'warning'
		elsif @compilation_output.blank?
			event.event = 'success'
		end
		event.save


		render json: {cmd: @cmd, output: @compilation_output, message: @message}, status: :ok
	end	


	# GET /tokenize.json
	def tokenize
		code = params[:code]
		cmd = "clang++-3.9 -cc1 -dump-tokens -Wfatal-errors -stdlib=libc++ -std=c++14"
		output = code.tokenizer_output('c++')
		tokens = code.tokenize('c++')
		render json: {cmd: cmd, output: output, tokens: tokens}, status: :ok
	end


	# GET /possible_fixes
	def possible_fixes
		@autoexperiment = false
		@ae_scores = {}
		# need code in order to rank by similarity
		# or tokens, at least
		@compiler = Compiler.find(1)
		code = params[:code]
		compilation = code.compile('c++')
		@compilation_output, @compilation_status = compilation[0], compilation[1]
		message = @compilation_output.extract_diagnostic_message('c++') # extract dm
		@patches = {}
		@fixes = []
		@fix_examples = []
		Rails.logger.debug '@compilation_status'
		Rails.logger.debug @compilation_status 

		if @compilation_status.exitstatus == 1
			@patches = build_possible_patches(code,message)
			@fixes = @patches.keys
			@fix_examples = []
			@fixes.each do |fix|
				@fix_counts = @patches[fix].size
				@fix_examples << @patches[fix]
			end
			@fix_examples.map! do |examples|
				examples = examples.first(10)
			end
		end
		
  	respond_to do |format|
			format.js 
			#format.json { render json: {fixes: @fixes, fix_examples: @fix_examples}, status: :ok}
		end
	end 

	# GET /autoexperiment
	def autoexperiment
		@autoexperiment = true
		@compiler = Compiler.find(1)
		code = params[:code]
		compilation = code.compile('c++')
		@compilation_output, @compilation_status = compilation[0], compilation[1]
		message = @compilation_output.extract_diagnostic_message('c++') # extract dm
		@ae_scores, @patches = *do_autoexperiment(code,message)
		#Rails.logger.debug '@ae_scores.size'
		#Rails.logger.debug @ae_scores.inspect
		#Rails.logger.debug '@patches.size'
		#Rails.logger.debug @patches.size
		@fixes = @patches.keys
		@fix_examples = []
		@fixes.each do |fix|
			@fix_counts = @patches[fix].size
			@fix_examples << @patches[fix]
		end
		@fix_examples.map! do |examples|
			examples = examples.first(10)
		end

		respond_to do |format|
			#format.js 
  		format.js { render :action => "possible_fixes" }
			#format.json { render json: {fixes: @fixes, fix_examples: @fix_examples}, status: :ok}
		end
	end

	private 

		def build_possible_patches(code,message)
			Rails.logger.debug __method__
			patches = {}
			tokens = code.tokenize('c++')
			message = @compilation_output.extract_diagnostic_message('c++') # extract dm
			#Rails.logger.debug tokens
			#Rails.logger.debug message 
			key1 = Digest::SHA1.hexdigest("/patches/#{message}/#{tokens.join()}")
			Rails.logger.debug 'key1'
			Rails.logger.debug key1
			#Rails.logger.debug 'Rails.cache.inspect'
			#Rails.logger.debug Rails.cache.inspect
			#Rails.logger.debug 'Rails.cache.read(key1)'
			#Rails.logger.debug Rails.cache.read(key1)
			Rails.cache.fetch(key1, compress: true) do
				# FIND related examples	grouped by patch
				key2 = Digest::SHA1.hexdigest("/examples/related_grouped_by_patch/#{message}")
				Rails.logger.debug 'key2'
				Rails.logger.debug key2
				Rails.cache.fetch(key2, compress: true) do
					patches = Example.related_grouped_by_patch(message)
					Rails.logger.debug 'FRESH patches'
					Rails.logger.debug patches.size
				end
				Rails.logger.debug 'CACHED patches'
				Rails.logger.debug patches.size

				Rails.logger.debug 'Filter possible patches'
				patches.delete_if do |p,es|
					status = false
					#Rails.logger.debug 'patch'
					#Rails.logger.debug p
					ptokens = []
					insts = p.split(', ')
					#Rails.logger.debug 'insts'
					#Rails.logger.debug insts
					insts.each do |inst|
						#Rails.logger.debug 'inst'
						#Rails.logger.debug inst
						op_token = inst.split(' ')
						#Rails.logger.debug 'op_token'
						#Rails.logger.debug op_token
						if op_token[0] == 'Delete'
							ptokens << op_token[1]
							#Rails.logger.debug 'tokens '
							#Rails.logger.debug tokens 
							#Rails.logger.debug 'ptokens'
							#Rails.logger.debug ptokens
							status = true if !tokens.include?(op_token[1])
						end
					end
					status
				end
				Rails.logger.debug 'patches'
				Rails.logger.debug patches.size

				Rails.logger.debug 'SCORE examples'
				patches.each do |p,es|
					es.map! do |e|
						e.overlap_coefficient = e.buggy_tokens.overlap_coefficient(tokens)
						e.levenshtein_distance = Levenshtein.distance(code, e.buggy)
						e
					end
				end
				Rails.logger.debug 'patches'
				Rails.logger.debug patches.size
							
				Rails.logger.debug 'RANK patches'
				patches.each do |k,v|
					# Sort examples within fix_group
					v.sort_by! do |e| 
						#e.overlap_coefficient
						[-1 * e.overlap_coefficient, -1 * e.levenshtein_distance]
					end
					#v.reverse!
				end	 
				Rails.logger.debug 'patches'
				Rails.logger.debug patches.size
				patches
			end # Rails.cache.fetch 
		end


		def do_autoexperiment(code,message)
			tokens = code.tokenize('c++')
			key = Digest::SHA1.hexdigest("autoexperiment/#{message}/tokens")
			Rails.cache.fetch(key) do
				patches = {}
				patches = build_possible_patches(code,message)
				ae_scores = {}
				patches = patches.each do |p,es|
					#Rails.logger.debug "AUTOEXPERIMENT: #{p}"
					ae_scores[p] = 0
					if tokens.size <= 150
						ae_scores[p] = code.autoexperiment(p)		
					end
					Rails.logger.debug "RESULT:\t#{p}:\t#{ae_scores[p]}"
				end
				patches = patches.sort_by do |p,es|
					[-1 * ae_scores[p], -1 * es.first.overlap_coefficient, es.first.levenshtein_distance]
				end
				patches = patches.to_h
				keys = patches.keys
				ae_scores.sort_by { |k, _| keys.index(k) }.to_h
				[ae_scores, patches]
			end # Rails.cache.fetch 
		end

		# Never trust parameters from the scary internet, only allow the white list through.
    def compile_params
			params.permit(:buggy, :utf8, :page)
    end

end # class
