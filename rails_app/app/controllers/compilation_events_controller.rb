class CompilationEventsController < ApplicationController
  before_action :set_compilation_event, only: [:show, :edit, :update, :destroy]

  # GET /compilation_events
  # GET /compilation_events.json
  def index
    @compilation_events = CompilationEvent.all.order('created_at DESC')
  end

  # GET /compilation_events/1
  # GET /compilation_events/1.json
  def show
  end

  # GET /compilation_events/new
  def new
    @compilation_event = CompilationEvent.new
  end

  # GET /compilation_events/1/edit
  def edit
  end

  # POST /compilation_events
  # POST /compilation_events.json
  def create
    @compilation_event = CompilationEvent.new(compilation_event_params)

    respond_to do |format|
      if @compilation_event.save
        format.html { redirect_to @compilation_event, notice: 'Compilation event was successfully created.' }
        format.json { render :show, status: :created, location: @compilation_event }
      else
        format.html { render :new }
        format.json { render json: @compilation_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /compilation_events/1
  # PATCH/PUT /compilation_events/1.json
  def update
    respond_to do |format|
      if @compilation_event.update(compilation_event_params)
        format.html { redirect_to @compilation_event, notice: 'Compilation event was successfully updated.' }
        format.json { render :show, status: :ok, location: @compilation_event }
      else
        format.html { render :edit }
        format.json { render json: @compilation_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /compilation_events/1
  # DELETE /compilation_events/1.json
  def destroy
    @compilation_event.destroy
    respond_to do |format|
      format.html { redirect_to compilation_events_url, notice: 'Compilation event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_compilation_event
      @compilation_event = CompilationEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def compilation_event_params
      params.require(:compilation_event).permit(:user_id, :code, :output, :command, :error, :event, :examples_count, :fixes_count)
    end
end
