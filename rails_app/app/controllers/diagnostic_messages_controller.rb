require 'base64'

class DiagnosticMessagesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_diagnostic_message, only: [:show, :edit, :update, :destroy]


  # GET /diagnostic_messages
  # GET /diagnostic_messages.json
  def index
		@compiler = Compiler.find(params[:compiler_id])
		@dms = DiagnosticMessage.where(compiler_id: @compiler.id).paginate(:page => params[:page]).order('id DESC')
  end


	def search
		q = params[:q]
		@empty_query = (q.nil? or q.blank?)
		Rails.logger.debug @empty_query
		if !@empty_query
			@dms = DiagnosticMessage.unscoped.search_by_message(q).with_pg_search_highlight.paginate(:page => params[:page])
		else
			@dms = DiagnosticMessage.unscoped.order("RANDOM()").paginate(:page => params[:page])
		end
	end


  # GET /diagnostic_messages/1
  # GET /diagnostic_messages/1.json
  def show
		@compiler = Compiler.find(params[:compiler_id])
		if @diagnostic_message.nil?
			if !params[:m].nil?
				@message = params[:m] 
			else
				@message = 'Unkown Diagnostic Message'
			end
			render 'unknown'
		end
		@examples = @diagnostic_message.examples.unscoped.paginate(:page => params[:page])
  end


	# GET /diagnostic_messages/1/embed
  # GET /diagnostic_messages/1/embed.json
	def embed
		@compiler = Compiler.find(params[:compiler_id])
		#begin 
    	@diagnostic_message = DiagnosticMessage.find(params[:diagnostic_message_id])
			#Rails.logger.debug @diagnostic_message.inspect
			@compiler = @diagnostic_message.compiler
			#Rails.logger.debug @compiler.inspect
			if !params[:c].nil?
				source_code = Base64.decode64(params[:c])
				#Rails.logger.debug "source_code"
				#Rails.logger.debug source_code	
				# TODO: tokenize using clang
				tokens = source_code.tokenize('c++')
				#Rails.logger.debug "tokens"
				#Rails.logger.debug tokens 
				@diagnostic_message.examples.map do |e|
					#Rails.logger.debug tokens
					e_tokens = e.buggy.tokenize	
					#Rails.logger.debug e_tokens
					a = tokens.size
					b = e_tokens.size
					min = a > b ? a : b
					intersection = tokens & e_tokens
					#Rails.logger.debug 'intersection'
					#Rails.logger.debug intersection
					e.overlap_coefficient = (intersection.size / min.to_f) 
					#Rails.logger.debug 'e.overlap_coefficient'
					#Rails.logger.debug e.overlap_coefficient
					e.overlap_coefficient
				end
				@examples = @diagnostic_message.examples.to_ary
				#Rails.logger.debug "UNSORTED: first = #{@examples[0].id}"
				#Rails.logger.debug "UNSORTED: last = #{@examples[-1].id}"
				@examples.sort_by! do |e| 
					e.overlap_coefficient
				end
				@examples.reverse!
				#Rails.logger.debug "SORTED: first = #{@examples[0].id}"
				#Rails.logger.debug "SORTED: last = #{@examples[-1].id}"
			else
				
			end
		#rescue
		#	@diagnostic_message = nil
		#	if @diagnostic_message.nil?
		#		if !params[:m].nil?
		#			@message = params[:m] 
		#		else
		#			@message = 'Unkown Diagnostic Message'
		#		end
		#		render 'unknown_embed'
		#	end
		#end
	end


  # GET /diagnostic_messages/new
  def new
		@compiler = Compiler.find(params[:compiler_id])
    @diagnostic_message = DiagnosticMessage.new
  end


  # GET /diagnostic_messages/1/edit
  def edit
		@compiler = Compiler.find(params[:compiler_id])
  end


  # POST /diagnostic_messages
  # POST /diagnostic_messages.json
  def create
    @diagnostic_message = DiagnosticMessage.new(diagnostic_message_params)
		@diagnostic_message.compiler_id = params[:compiler_id]
		@diagnostic_message.created_by = current_user.id

    respond_to do |format|
      if @diagnostic_message.save
        format.html { redirect_to compiler_diagnostic_message_url(params[:compiler_id], @diagnostic_message), notice: 'Diagnostic message was successfully created.' }
        format.json { render :show, status: :created, location: @diagnostic_message }
      else
        format.html { render :new }
        format.json { render json: @diagnostic_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diagnostic_messages/1
  # PATCH/PUT /diagnostic_messages/1.json
  def update
    respond_to do |format|
      if @diagnostic_message.update(diagnostic_message_params)
        format.html { redirect_to @diagnostic_message, notice: 'Diagnostic message was successfully updated.' }
        format.json { render :show, status: :ok, location: @diagnostic_message }
      else
        format.html { render :edit }
        format.json { render json: @diagnostic_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diagnostic_messages/1
  # DELETE /diagnostic_messages/1.json
  def destroy
		#TODO: Only admin can delete
		render nothing: true, status: :unauthorized
    @diagnostic_message.destroy
    respond_to do |format|
      format.html { redirect_to diagnostic_messages_url, notice: 'Diagnostic message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diagnostic_message
			begin
      	@diagnostic_message = DiagnosticMessage.find(params[:id])
			rescue
				Rails.logger.debug 'could not find dm'
      	@diagnostic_message = nil
			end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diagnostic_message_params
      params.require(:diagnostic_message).permit(:compiler_id, :name, :message, :created_by)
    end

end
