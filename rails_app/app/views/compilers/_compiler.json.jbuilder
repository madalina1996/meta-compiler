json.extract! compiler, :id, :family, :name, :description, :url, :created_by, :created_at, :updated_at
json.url compiler_url(compiler, format: :json)