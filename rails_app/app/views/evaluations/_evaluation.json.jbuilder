json.extract! evaluation, :id, :user_id, :status, :code, :questionnaire, :tutorial_quiz, :created_at, :updated_at
json.url evaluation_url(evaluation, format: :json)