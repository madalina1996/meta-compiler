json.extract! snippet, :id, :snippet, :diagnostic_message_id, :tokens, :compiler_output, :source, :created_at, :updated_at
json.url snippet_url(snippet, format: :json)