json.extract! user, :id, :provider, :uid, :email, :name, :first_name, :last_name, :location, :image_url, :url, :created_at, :updated_at
json.url user_url(user, format: :json)