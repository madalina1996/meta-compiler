class DiagnosticMessage < ApplicationRecord
  include PgSearch
  self.per_page = 20


  default_scope { order('bing_search_result_count DESC, examples_count DESC, message ASC') }
	belongs_to :compiler, counter_cache: true
	belongs_to :template, counter_cache: true
	has_many :examples
	has_many :snippets
	belongs_to :creator, class_name: User, foreign_key: 'created_by'
	validates :compiler_id, presence: true

	# 0 (the default) ignores the document length
	# 1 divides the rank by 1 + the logarithm of the document length
	# 2 divides the rank by the document length
	# 4 divides the rank by the mean harmonic distance between extents
	# 8 divides the rank by the number of unique words in document
	# 16 divides the rank by 1 + the logarithm of the number of unique words in document
	# 32 divides the rank by itself + 1
  pg_search_scope :search_by_message, 
									:against => :message, :using => {
                 		:tsearch => {
											:prefix => true, 					# match prefix
											:dictionary => "simple", 	# literal matching, no stemming
											:normalization => (2 + 16),
											:highlight => {
                        :start_sel => '<b>',
                        :stop_sel => '</b>'
                      }
										}
                  }


	# Convert message to search engine query string.
	# Example 1) '_V' does not point into a class 
	# => "does not point into a class"
	# Example 2) expected '(' after 'if' 
	# => "expected" "after"
	# params: none
	def to_query_string
		Rails.logger.debug __method__
		delim = '*****'
		if !self.message.nil?
			message = self.message.dup
			#Rails.logger.debug "message"
			#Rails.logger.debug "#{message.inspect}"
			indices = message.enum_for(:scan,/'/).map { Regexp.last_match.begin(0) }
			if indices.size % 2 != 0
				p_of_i = []
				if !message.index("function's").nil?
					p_of_i = indices[1..2]	
				elsif !message.index("constructor's").nil?
					p_of_i = indices[1..2]	
				elsif !message.index("attribute's").nil?
					p_of_i = indices[0..1]	
				else 
					return nil
				end
				message[p_of_i[0]..p_of_i[1]] = delim
				Rails.logger.debug "message"
				Rails.logger.debug "#{message.inspect}"
			else
				while !message.index("'").nil?
					p_of_i = message.enum_for(:scan,/'/).map { Regexp.last_match.begin(0) }.first(2)
					message[p_of_i[0]..p_of_i[1]] = delim
					Rails.logger.debug "message"
					Rails.logger.debug "#{message.inspect}"
				end
			end
			phrases = message.split(delim)
			phrases.map! { |phrase| phrase.strip } 
			Rails.logger.debug "#{self.message}"
			#Rails.logger.debug "#{message}"
			Rails.logger.debug "#{phrases.inspect}"
			phrases.map! do |phrase|
				if !phrase.blank?
					phrase.prepend('"')
					phrase << '"'
				end
			end
			phrases = phrases.compact
			Rails.logger.debug "#{phrases.inspect}"
			return phrases.join(' ')
		end
		return nil
	end


	
end
