require 'digest/sha1'
require 'open3'

class Example < ApplicationRecord
  default_scope { order('char_length(fixed) ASC, fixed ASC') }
	belongs_to :diagnostic_message, counter_cache: true
	belongs_to :creator, class_name: User, foreign_key: 'created_by'
	belongs_to :template
	belongs_to :compiler
	validates :fixed, presence: true
	validates :buggy, presence: true
	#validates :fixed_tokens_sha1, presence: true
	#validates :buggy_tokens_sha1, presence: true, uniqueness: true
	#validates_uniqueness_of :buggy_tokens_sha1
  #before_validation :tokenize_and_hash
	validate :fix_uniqueness
	attr_accessor :overlap_coefficient
	attr_accessor :levenshtein_distance
	attr_accessor :diff

	def fix_uniqueness
		Rails.logger.debug __method__
		tokenize_and_hash
		Rails.logger.debug self.inspect
		if !self.buggy_tokens_sha1.nil?
			e = Example.find_by(diagnostic_message_id: self.diagnostic_message_id, buggy_tokens_sha1: self.buggy_tokens_sha1, fixed_tokens_sha1: self.fixed_tokens_sha1)
			if !e.nil?
				if e.id != self.id
					Rails.logger.debug e.inspect
					errors.add(:fix_uniqueness, "Similar fix already exists. See example ##{e.id}.")
				end
			end
		else
			errors.add(:buggy_tokens_sha1, "Buggy code sha1 missing.")
		end
	end


	def tokenize_and_hash
		Rails.logger.debug __method__
		self.buggy_tokens = self.buggy.tokenize()
		self.fixed_tokens = self.fixed.tokenize()
		token_str = self.buggy_tokens.join(' ')
		self.buggy_tokens_sha1 = Digest::SHA1.digest(token_str)	
		token_str = self.fixed_tokens.join(' ')
		self.fixed_tokens_sha1 = Digest::SHA1.digest(token_str)	
	end


	# Tokenize both buggy and fixed code and save
	def tokenize_buggy_and_fixed
		self.buggy_tokens = self.buggy.tokenize()
		self.fixed_tokens = self.fixed.tokenize()
		self.save
	end


	# Tokenize buggy code and save
	def tokenize_buggy
		self.buggy_tokens = self.buggy.tokenize()
		self.save
	end


	# Tokenize fixed code and save
	def tokenize_fixed
		self.fixed_tokens = self.fixed.tokenize()
		self.save
	end


	def hash_buggy_and_fixed_tokens
		token_str = self.buggy_tokens.join(' ')
		self.buggy_tokens_sha1 = Digest::SHA1.digest(token_str)	
		token_str = self.fixed_tokens.join(' ')
		self.fixed_tokens_sha1 = Digest::SHA1.digest(token_str)	
		self.save
	end

	def hash_buggy_tokens
		token_str = self.buggy_tokens.join(' ')
		self.buggy_tokens_sha1 = Digest::SHA1.digest(token_str)	
		self.save
	end

	def hash_fixed_tokens
		token_str = self.fixed_tokens.join(' ')
		self.fixed_tokens_sha1 = Digest::SHA1.digest(token_str)	
		self.save
	end

	def compile
		self.buggy_compilation_output = self.buggy.compile[0]
		self.buggy_compilation_status = self.buggy.compile[1].exitstatus
		self.fixed_compilation_output = self.fixed.compile[0]
		self.fixed_compilation_status = self.fixed.compile[1].exitstatus
		self.save
	end


	def compile_and_save
		cmd = "clang++-3.9 -c -Wfatal-errors -std=c++14 -stdlib=libc++ -x c++ -"
		stdout_and_stderr_str, status = Open3.capture2e(cmd, stdin_data: self.buggy)
		Rails.logger.debug stdout_and_stderr_str
		self.compiler_output = stdout_and_stderr_str
		self.save
	end


	def compile_and_save_fixed
		cmd = "clang++-3.9 -c -Wfatal-errors -std=c++14 -stdlib=libc++ -x c++ -"
		stdout_and_stderr_str, status = Open3.capture2e(cmd, stdin_data: self.fixed)
		Rails.logger.debug stdout_and_stderr_str
		self.compiler_output_fixed = stdout_and_stderr_str
		self.save
	end



	def update_diagnostic_message_id
		Rails.logger.debug __method__
		return false if !self.anomalous_dm?
		extracted_message = self.extract_diagnostic_message(self.compiler_output)
		# search for dm to get id
		dm = DiagnosticMessage.find_by(message: extracted_message)
		Rails.logger.debug 'dm.inspect'
		Rails.logger.debug dm.inspect
		if dm.nil?
			admin = User.find(1)
			compiler_id = self.diagnostic_message.compiler_id
			dm = DiagnosticMessage.create(compiler_id: compiler_id, message:extracted_message, created_by: admin.id)
			Rails.logger.debug 'dm.inspect'
			Rails.logger.debug dm.inspect
		end
		self.diagnostic_message_id = dm.id	
		if self.save
			Rails.logger.debug 'SUCCESS: Updated diagnostic_message_id.'	
			return true
		else
			Rails.logger.debug 'self.errors.inspect'
			Rails.logger.debug self.errors.inspect
		end
		
		Rails.logger.debug 'ERROR: Failed to update diagnostic_message_id.'	
		return false
	end


	def extract_diagnostic_message(compiler_output)
		Rails.logger.debug __method__
		return nil if compiler_output.nil?
		# parse line with "error:" to get diagnostic message
		dm = ""
		compiler_output.each_line do |line|
			if line =~ /error\:/ 
				dm = line.split("error:")[1].strip
			end                                                                             
		end 

		return dm		
	end


	def anomalous_dm?
		Rails.logger.debug __method__
		a = self.extract_diagnostic_message(self.compiler_output) 
		if !a.nil?
			b = self.diagnostic_message.message
			Rails.logger.debug 'a = self.extract_diagnostic_message'
			Rails.logger.debug a.inspect
			Rails.logger.debug 'b = self.diagnostic_message.message'
			Rails.logger.debug b.inspect
			Rails.logger.debug 'a != b'
			Rails.logger.debug a != b
			return a != b
		end
		return false
	end

	def bad_fix?
		Rails.logger.debug __method__
		return self.fixed_compilation_status == 0
	end

	def diff_tokens
		change = 0
		token = ""
		if self.buggy_tokens.size > self.fixed_tokens.size
			change = -1 # delete
			token = self.buggy_tokens.subtract_once(self.fixed_tokens)
		elsif self.buggy_tokens.size < self.fixed_tokens.size
			change = 1 # insert
			token = self.fixed_tokens.subtract_once(self.buggy_tokens)
		else
			change = 0 # replace	
			buggy_token = self.buggy_tokens.subtract_once(self.fixed_tokens)
			fixed_token = self.fixed_tokens.subtract_once(self.buggy_tokens)
			token = buggy_token + fixed_token
		end
		self.token_diff_change = change
		self.token_diff_token = token
	end


	def set_patch
		self.patch = self.token_diff_message
		self.save	
	end


	def token_diff_message
		#Rails.logger.debug __method__
		#Rails.logger.debug self.id
		message = "Cannot compute token diff."
		self.diff_tokens
		op = ''

		if !self.token_diff_change.nil?
			message = ""
			if self.token_diff_change == -1	
				op = "Delete"
			elsif self.token_diff_change == 1
				op = "Insert"
			elsif self.token_diff_change == 0
				op = "Replace"
			end
			#Rails.logger.debug self.token_diff_token.inspect
			tokens = JSON.parse(self.token_diff_token)
			#Rails.logger.debug tokens.inspect
			#Rails.logger.debug tokens.inspect
			if self.token_diff_change == 0
				message = op + ' ' + tokens.join(' with ')
			else
				tokens.each do |t|
					message += "#{op} #{t}, "
				end
				message[-2..-1] = ''
			end
		end

		return message
	end


	def format
		newb = self.buggy.format('Chromium')
		newf = self.fixed.format('Chromium')
		return false if newb == "No such file or directory"	
		return false if newf == "No such file or directory"	
		self.buggy = newb 
		self.fixed = newf
	end


	def format_and_save
		self.format
		self.save
	end


	# For a given piece of code, find all related examples
	# Related = trigger the same compiler error template 
	def self.related(message)
		Rails.logger.debug __method__
		examples = []

		# Find examples that trigger same error or error template 
		dm = DiagnosticMessage.find_by(message: message)
		if !dm.nil? and !dm.template_id.nil?
			template = Template.find(dm.template_id)
			if !template.nil?
				Rails.logger.debug 'template.diagnostic_messages.size'
				Rails.logger.debug template.diagnostic_messages.size
				examples = template.examples
				examples = examples.to_ary
			else # template.nil?
				Rails.logger.debug 'ERROR: template.nil?'	
				Rails.logger.debug 'dm.inspect'	
				Rails.logger.debug dm.inspect
			end #!template.nil?
		else # @dm.nil?	
			template_temp = message.to_template
			Rails.logger.debug 'template_temp.inspect'
			Rails.logger.debug template_temp.inspect
			templates = Template.unscoped.search_by_template(template_temp)
			Rails.logger.debug 'templates.inspect'
			Rails.logger.debug templates.inspect
			templates.each do |t|
				examples.concat(t.examples)
			end
		end # if !@dm.nil?
		return examples
	end #related


	def self.related_grouped_by_patch(message)
		Rails.logger.debug __method__
		examples = []
		patches = {}
		key = Digest::SHA1.hexdigest("examples/related/#{message}")
		Rails.logger.debug 'key'
		Rails.logger.debug key
		Rails.cache.fetch(key, compress: true) do
			examples = Example.related(message)
			# GROUP examples
			patches = examples.group_by { |e| e.patch }	
			patches.to_h
		end
	end



	def self.same_token_size
		es = []
		Example.unscoped.order(:id).each do |e|
			if e.buggy_tokens.size == e.fixed_tokens.size
				es << e
			end
		end	
		return es
	end


	def self.same_token_size_summary
		es = self.same_token_size
		patches = {}
		es.each do |e|
			if patches.has_key?(e.patch)
				patches[e.patch] << e.id
			else
				patches[e.patch] = []
			end
		end	
		return patches
	end


	def set_sizes
		self.buggy_loc = buggy.lines.count
		self.fixed_loc = fixed.lines.count
		self.buggy_tokens_size = self.buggy_tokens.size
		self.fixed_tokens_size = self.fixed_tokens.size
		self.save
	end
	#private


	def self.buggy_not_buggy
		es = Example.where(buggy_compilation_output:'',buggy_compilation_status:0)	
		return es
	end

	def set_diff
		Diffy::Diff.new(self.buggy,self.fixed).to_s(:html)
	end


end
