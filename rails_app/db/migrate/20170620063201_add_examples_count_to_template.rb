class AddExamplesCountToTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :templates, :examples_count, :integer, default: 0, null: false
  end
end
