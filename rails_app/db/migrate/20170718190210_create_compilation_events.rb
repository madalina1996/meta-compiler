class CreateCompilationEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :compilation_events do |t|
      t.integer :user_id
      t.text :code
      t.text :output
      t.string :command
      t.text :error
      t.string :event
      t.integer :examples_count
      t.integer :fixes_count

      t.timestamps
    end
  end
end
