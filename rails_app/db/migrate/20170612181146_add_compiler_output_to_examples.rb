class AddCompilerOutputToExamples < ActiveRecord::Migration[5.0]
  def change
    add_column :examples, :compiler_output, :text
  end
end
