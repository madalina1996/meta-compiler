class AddCompilerOutputFixedToExample < ActiveRecord::Migration[5.0]
  def change
    add_column :examples, :compiler_output_fixed, :text
  end
end
