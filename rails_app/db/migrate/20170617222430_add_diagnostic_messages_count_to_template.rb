class AddDiagnosticMessagesCountToTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :templates, :diagnostic_messages_count, :integer
  end
end
