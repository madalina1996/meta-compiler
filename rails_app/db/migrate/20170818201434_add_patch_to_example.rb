class AddPatchToExample < ActiveRecord::Migration[5.0]
  def change
    add_column :examples, :patch, :string
  end
end
