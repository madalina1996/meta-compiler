class AddSha1ToExamples < ActiveRecord::Migration[5.0]
  def change
		add_column :examples, :buggy_tokens_sha1, :binary, :limit => 20.bytes
		add_column :examples, :fixed_tokens_sha1, :binary, :limit => 20.bytes
  end
end
