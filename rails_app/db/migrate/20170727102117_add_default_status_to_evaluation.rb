class AddDefaultStatusToEvaluation < ActiveRecord::Migration[5.0]
  def change
		change_column :evaluations, :status, :integer, default: 0
  end
end
