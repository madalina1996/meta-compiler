class AddExamplesCountToCompiler < ActiveRecord::Migration[5.0]
  def change
    add_column :compilers, :examples_count, :integer, default: 0, null: false
  end
end
