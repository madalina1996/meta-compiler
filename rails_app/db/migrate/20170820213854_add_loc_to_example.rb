class AddLocToExample < ActiveRecord::Migration[5.0]
  def change
    add_column :examples, :buggy_loc, :integer
    add_column :examples, :fixed_loc, :integer
    add_column :examples, :buggy_tokens_size, :integer
    add_column :examples, :fixed_tokens_size, :integer
  end
end
