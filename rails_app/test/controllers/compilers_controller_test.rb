require 'test_helper'

class CompilersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @compiler = compilers(:one)
  end

  test "should get index" do
    get compilers_url
    assert_response :success
  end

  test "should get new" do
    get new_compiler_url
    assert_response :success
  end

  test "should create compiler" do
    assert_difference('Compiler.count') do
      post compilers_url, params: { compiler: { created_by: @compiler.created_by, description: @compiler.description, family: @compiler.family, name: @compiler.name, url: @compiler.url } }
    end

    assert_redirected_to compiler_url(Compiler.last)
  end

  test "should show compiler" do
    get compiler_url(@compiler)
    assert_response :success
  end

  test "should get edit" do
    get edit_compiler_url(@compiler)
    assert_response :success
  end

  test "should update compiler" do
    patch compiler_url(@compiler), params: { compiler: { created_by: @compiler.created_by, description: @compiler.description, family: @compiler.family, name: @compiler.name, url: @compiler.url } }
    assert_redirected_to compiler_url(@compiler)
  end

  test "should destroy compiler" do
    assert_difference('Compiler.count', -1) do
      delete compiler_url(@compiler)
    end

    assert_redirected_to compilers_url
  end
end
