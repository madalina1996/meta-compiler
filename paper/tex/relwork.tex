\section{Related Work}
\label{sec:relwork}

Numerous tools, techniques, and even programming languages have been developed
to help programmers with compiler errors~\cite{mciver2000wppig, omar2017popl}.
To the best of our knowledge, this is the first approach that precomputes
compilation repairs with minimal examples, and presents them to the user as
candidate patches.

\subsection{Compiler Error Augmentation}

Early, compilation repair examples were created \emph{manually}.
Over a decade ago, Kummerfield \etal developed a ``web-based reference guide''
where ``each error message is explained with examples highlighting the problem
and at least one possible correction''~\cite{kummerfield2003acce}.
They gave students 8 uncompilable programs and asked them to fix them using
their tool.
They found that the tool helped novice students perfrom similarly to expert
students.
Toomey~\cite{toomey2011web} developed a modified version of the BlueJ IDE that
provided English advice on how to fix common errors which sometimes
included snippets.

Recently, repair examples have been \emph{crowdsourced}.
Hartmann \etal developed HelpMeOut, a plugin to a Java IDE, that
crowdsources the creation of ``solutions'' (compilation repair examples) to its
users~\cite{hartman2010chi}.  It is rather comprehensive in that it explores
ranking repair examples,  system that collects ``succesful solutions'' from
users, supports manually adding English explanations, and even voting.
Mujumdar \etal presented Crowd::Debug, a tool that aimed to apply HelpMeOut's
approach to dynamic programming languages, namely Ruby ~\cite{mujumdar2011chi}.

The strength of crowdsourcing is that the collected repair examples are
``natural''.  Though, our user study suggests that \emph{synthetic} examples
are helpful as well.  As an approach, crowdsourcing has serious limitations.
The collection needs to be bootstrapped to be useful to initial users.  The
collected examples may not cover the entire compiler space and may saturate
certain errors.  User adoption may also be low because people usually do not
want to or are not allowed to share their code.

%\begin{enumerate}

%\item Privacy: Many companies forbid their software engineers to upload code to
%third party services like HelpMeOut.  

%\item Bootstrap: HelpMeOut starts with no repair examples, and its initial
%users gain no benefit.  Suppose we want to support a new compiler.  With
%HelpMeOut, one needs to start crowdsourcing from scratch.

%\item Coverage: With HelpMeOut, infrequent errors will have no/few example
%fixes.  The frequent errors, though probably important, will be saturated.
%Infrequent errors may also be the most difficult for users.  

%\end{enumerate}

\textsc{CompAssist} differs in that repair examples are synthesized completely
automatically via our \textsc{Fuzz-and-Reduce} approach.  This allows it to
cover a greater portion of the compiler error space, and to provide multiple
examples for each compiler error.  Since we create high-level patches, this
allows us to not only augment compiler errors but to attempt automatic repair!

Our user study and the results of related tools support the hypothesis that
good examples can help programmers.
However, there is still ongoing debate regarding the effectiveness of
augmenting compiler errors with repair
examples~\cite{denny2014iticse,pettit2017sigcse}.

\subsection{Automated Program Repair}

Most well-known program repair techniques focus on logic errors while
\textsc{CompAssist} deals with compilation errors.  
For example, recently, Yi \etal studied the feasibility of using automated
program repair of in introductory programming assignments but focused solely on
logic errors~\cite{yi2017fse}.

\textbf{Search-based:} 
Traditional search-based automated program repair techniques, like
GenProg~\cite{legoues2012tse, weimer2009icse} and
\textsc{Par}~\cite{kim2013icse}, focus on repairing logic errors.  They depend
on a fitness function and a test suite to guide their search for a correct
program.  The intuition is that if a patch leads to a mutant that passes more
test cases, then that direction should be further pursued.  We are not aware of
any extension of these techniques that successfully constructs a fitness
function based primarily on compiler output and independent of a test suite.
We believe the difficulty lies in the coarseness of the compiler output; a
program either compiles or not.  Worst, if a patch triggers a different error,
then it is unclear if the new program is closer to being compilable.

\textbf{Model-based:}  Learned models can help identify and fix
compilation and logic defects.  
Campbell \etal trained an $n$-gram model over compilable Java programs, and
evaluated it by locating errors in a synthetic corpus of uncompilable
code~\cite{campbell2014msr}.  
Long \etal developed Phropet, a system that learns a probabilistic model from
successful patches.  It uses this model to assign probabilities to candidate
patches~\cite{long2016popl}.
Pu \etal designed and trained seq2seq neural network model sequence on correct
student programs and used it to correct syntactic and logic errors of incorrect
programs~\cite{pu2016splash}.
Similarly, Gupta \etal trained a seq2seq model with attention with the design
objective of being task-independent.
TRACER is a recent effort that combines program analysis and deep
learning to fix compiler errors in student programs~\cite{ahmed2018icse}. 


\textbf{Rule-based:}
Other approaches learn rules (or transformations) from repair examples. 
NoFAQ learns
transformations rules from crowdsources examples of ``buggy and repaired
[shell] commands''.  With these rules, it can repair ``buggy [shell]
commands''~\cite{dantoni2017fse}.
Refazer is a similar technique that learns syntactic transformations from
``input-output examples''~\cite{rolim2017icse}. The learned transformations can
be useful in debugging and refactoring tasks.
Singh \etal introduced an approach that learns transformations expressed in a
error modeling language~\cite{singh2013pldi}.

\textsc{CompAssist} differs from most of the approaches described above in that
it is not data-driven; all repair examples are synthesized via fuzzing.  This
leads to a different search strategy because the precomputation step vastly
reduces the patch search space and enables quick auto-experimentation.  In our
current implementation, this space reduction means that we cannot always
suggest a patch acceptable to the user.  But we can always show possible
patches with minimal examples, something that users found to be a helpful
fallback.


\begin{comment}

Several works have studied interaction of programmers with compilers.
An eye-tracking study by \cite{barik2017icse} reported that
novices spend a substantial portion of a total task on reading
and understanding compiler error messages.
Some works aim to understand the debugging process
\cite{bohme2017fse,debug1,debug2}.
By collecting information from novice interactions with compilers,
\cite{marceau2011onward} have studied interaction of students
with assistive technologies in DrRacket.
In another work, \cite{marceau2011sigcse} propose a
language-independent rubric to evaluate effectiveness of
compiler error messages.
A work by \cite{06-jadud} identifies common errors
novice Java programmers made in a course
in BlueJ environment~\cite{BlueJ}.
\cite{seo2014icse} 
recognized the most costly compiler errors
in an industrial setting
by studying broken builds at Google.
A work by \cite{traver2010ahci} provides a thorough survey on
obstacles originated by compilers.

Our work is not concerned with
evaluating the efficacy of existing assistive technologies,
nor attempts to identify how error messages fail programmers.
Instead, we propose a novel, effective technique to automatically generate small fix examples. 
Despite the enhancements to readability and usability of compiler error messages, and studies
on what can be useful to programmers~\cite{Gauntlet,syntax-locate,08-novices,karel},
programmers might still find themselves unable to resolve an error.
Fix examples complement compiler error messages when errors fail to provide
effective assistance to programmers.

\subsection{Generalizing human-provided fixes}

NoFAQ\cite{dantoni2017fse} is a rule-based system that suggests possible
fixes for faulty CLI interactions.
It facilitates the synthesis of new rules from
crowdsourced examples of buggy and repaired commands.
Refazer \cite{rolim2017icse} learns AST transformations from the crowd to suggest
correcting fixes for student submissions with similar logical faults.
\cite{13-pldi-autograder} provide personalized corrections to student's incorrect solution based on a reference implementation of the assignment
\cite{hartman2010chi} designed an assistive system that relies on a crowd-sourced database of fix suggestions.
In an instrumented IDE, user interactions with the compiler are logged, and a successful fix for is added to the database to assist future users who encounter the same error.
In contrast, we neither rely on human fixes nor IDE instrumentation.
For every error triggered in our offline training phase,
we obtain at least one fix example for free, prior to any user interaction.
Reliance on centralized crowdsourcing in development tools limits adoption and is faced by serious privacy concerns and resistance by the developer community\footnote{``Python autocomplete-in-the-cloud tool Kite pushes into projects, gets stabbed with a fork'' \url{https://www.theregister.co.uk/2017/07/25/kite_flies_into_a_fork/}},
however, meta-compiler's ranking scheme could be enhanced with social features like voting and ranking, and the database of fixes could be augmented with crowd-sourced data.

% Technical part
%%%%%%%%%%%%%%%%%%%




\subsection{Program repair}

Starting with a faulty program, program repair techniques
try to look for a patch and derive a correct program from the original faulty
code. \cite{weimer2009icse, qi2014icse, leGoues2012tse, long2016popl}
Using data-driven or enumerative approaches,
these techniques explore a large search space
to find a mutant that passes the desired test
and doesn't exhibit the faulty behavior.
However, due to the size of the search space,
these approaches are not suitable for interactive settings.

To suggest possible, relevant and assistive fixes,
our method relies on careful decoupling of phases
and an appropriate error model
to avoid online exploration of possible patches.
In meta-compiler, the database of possible fixes is automatically obtained
in an offline setting
by mutating already existing compiler test-cases.
The error model (single token mutation)
restricts the search space to a smaller,
yet very usable subset of faulty programs,
and provides an easy fix for them.
This decoupling liberates us from the costly
quest to find a correct mutation for a particular faulty source-file,
allowing us to incorporate the offline knowledge in an interactive setting.

\subsection{Mutation-based testing}

Our offline and automatic technique to create the database
of possible fixes is related to the body of work
on compiler testing.

Csmith \cite{ChenGZWFER13taming, yang2011pldi} stress-tests compilers by
randomly generating large and complex C programs
and checking for inconsistent behavior across multiple compilers.
Csmith is known for its careful control to avoid generating programs with undefined behavior.
The randomly generated programs by Csmith are valid and compilable C files.

EMI testing \cite{le2014pldi} is another compiler testing technique
that does not generate code,
but consumes existing code (real or randomly generated)
and systematically modifies it.
Practical testing tools based on EMI mutate programs by inserting and deleting statements in unexecuted branches.
The frameworks based on EMI have revealed many bugs in both GCC and Clang/LLVM. Most of them are deep wrong code bugs.

For functional languages, there has been an extensive body of work on exhaustive or random test-case generators for compiler testing~\cite{
ClaessenH00quick,
ClaessenDP15generating,
DuregardJW12feat,
FetscherCPHF15making,
PalkaCRH11testing,
RuncimanNL08smallcheck}.
\cite{BoujarwahS97compiler} have conducted a thorough survey on generation techniques for compiler testing.

A recent work by \cite{17-pldi-skeletal-program-numeration}
generates mutants by preserving the skeleton of a program and
shifting the order variables within the same skeleton.
The key insight is that simple and small alterations to a program can
trigger diverse and intriguing behaviors in compilers.
Our technique to find fix examples
shares the same insight.
By applying simple mutations to GCC and LLVM test suite,
we have been able to find examples for a substantial portion
of all error messages in the C++ compiler.

\end{comment}

