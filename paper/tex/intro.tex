\section{Introduction}
\label{sec:intro}

% Notes that I did not include in FSE 2018 submission.
%Add illustrative example of reduce algorithm.
%Add a better example
%Introduction
%Benchmark for successful repairs.
%Can we use their benchmark?
%What is the error message coverage of Tracer and DeepFix?  
%Hope is that it is low.  Shares limitation of crowdsource.
%Unique in that we fix errors.
%Sequential autodenoiser.
%For each em, how can we trigger each error message.  Then we can use this dataset to suggest fixes.
%Why token level?  Character level too large and too much noise garbage.
%We want to be exhaustive with respect to errors not search space.
%campbell14msr: single-token, harder to break, less entropy and two-token break 


\begin{figure*}[t]
\includegraphics[width=\textwidth]{figures/t10.png}
\caption{\textsc{CompAssist} User Interface: A user types a C++ program, and
compiles it with the ``Compile'' button.  The system shows colorized compiler
output.  If the program triggers a compiler error, \textsc{CompAssist} suggests
repair patches with minimal examples to help a user understand the suggested
patch.  This C++ program was posted to StackOverflow by a person seeking help
understanding and repairing the compiler error.  We label this program T10 in
our user study.}

\label{fig:t10}
\end{figure*}


% Is this an interesting problem?
%Programmers of all levels struggle with compiler
Programmers of all levels struggle with compiler
errors~\cite{altadmri2015sigcse,hristova2003sigcse,jadud2006icer,kummerfield2003acce}.
Compilers display error messages to help users locate and resolve code defects.
However, these messages are notoriously difficult to understand, especially by
beginners, and can even be
misleading~\cite{barik2017icse,brown1983cacm,becker2015dublin,isa1983chi,traver2010achi}.
In addition to wasting time, programmers can become frustrated and beginners can
lose interest and even quit learning.
As programmers gain experience, they learn to recognize common messages,
remember root causes and repairs, and eventually struggle less. 
But even as professionals, software engineers continue to make
errors~\cite{barik2017icse,bohme2017fse,seo2014icse}.
For example, a recent case study at Google found that C++ and Java developers
fail to build programs, on average, $37.4$\% and $29.7$\% of the
time~\cite{seo2014icse}.  And it takes them a median of $5$ and $12$ minutes,
respectively, to resolve each error.

%How can we help programmers repair compilation errors?  
One further complication is that the quality of error messages and tool support
varies by programming language.
As a result, as highlighted by the Google study, for example, the compiler
errors made in C++ are different from those made in Java.  
One likely explanation is that Java developers (unlike C++ developers) rely on
Eclipse's QuickFix which continuously suggests repairs for Java 
errors~\cite{eclipse}.  
However, many programming languages lack this tool support.  
Thus, how can we help programmers resolve compiler errors who are using an
arbitrary programming language and compiler?

%Since switching programming languages or compilers is not usually an option,
%How can we help programmers repair errors while using their present environment?
One natural step is to improve the messages emitted by
compilers~\cite{marceau2011sigcse,marceau2011onward,nienaltowski2008sigcse,traver2010achi}.
Almost a decade ago, GCC developers completely replaced the C parser with a 
top-down parser to provide better "diagnostic messages"~\cite{fsf2004}.  Clang,
a newer compiler frontend, is well-known for focusing on ``expressive
diagnostic messages'' from the start~\cite{clang}.  However, many compiler
error messages remain cryptic across compilers, including Clang and GCC.

%However, compiler developers typically prioritize
%features like performance and language feature support, and pay less attention
%error messages~\cite{brown1983cacm,traver2010achi}.

Another approach is to attempt direct repair.
Recently, several data-driven automated repair techniques have been proposed.  
One group aims to learn shallow models of correct code and
repairs~\cite{campbell2014msr,long2016popl,pu2016splash,gupta2017aaai,ahmed2018icse}.
The hope is that these models can be used to locate defects and suggest repairs.
Another group aims to learn rules/transformations from repair
examples~\cite{dantoni2017fse,long2017fse,rolim2017icse}. 
While promising, these techniques have relatively low accuracy and require
data, repair examples.  
Often the data is mined from student homework submissions and thus can be a
small subset of all possible errors and biased toward beginner errors.

A different and promising direction is to augment existing error messages with
examples~\cite{barik2014icse,becker2016sigcse,flowers2004fie,hartman2010chi}.
A \underline{\emph{compilation repair example}} is a pair of programs that
illustrates how to repair a compilation error.  Seeing how a similar compiler
error has been solved before may help a programmer resolve his/her own error.
The challenge is: how to obtain and present these repair examples?  Given that
there many languages, many compilers, and that each error can be repaired in
different ways, manually crafting examples~\cite{kummerfield2003acce} is not
feasible.  Crowdsourcing~\cite{hartman2010chi,mujumdar2011chi} is more feasible
but is limited by bootstrapping, data bias, and privacy issues --- see
\autoref{sec:relwork}.

We present \textsc{CompAssist}, a system that synthesizes minimal compilation
repair examples automatically via a novel \textsc{Fuzz-and-Reduce} technique.
And, based on these examples, suggests possible patches for a compiler error.
Since the primary intention of a repair example is to help programmers,
examples should be \underline{\emph{minimal}} in the sense that they 
provide only sufficient code context for a programmer to understand it and
decide if the patch is applicable.  Irrelevant context can make a repair
example difficult to comprehend and ultimately not helpful.  


The \emph{key insight} of our approach is that it is much easier to break
compilable code than to repair uncompilable code.  
At a high-level, we start with a compilable program $c$, and randomly mutate
it.  If the mutated program $u$ is uncompilable, then we have a compilation
repair example!  
The \emph{intuition} is that if we do this over many different compilable
programs with random mutations, we can get many repair examples and 
high coverage of the compiler error space. 


%For example, the example shown in \autoref{fig:fe} is a reduced version of the
%example shown in \autoref{fig:fuzzed}.  
%The new patch $\Delta'$ is the same (except for shifted column position), but,
%clearly, it is much easier to understand the surrounding context.

We evaluated \textsc{CompAssist} on Clang++, a popular mainstream C++ compiler,
with respect to coverage (breadth and depth), example simplicity, and
helpfulness.  Even with the simplest token-level fuzzing strategy (single-token
mutations), our system is able to cover more than half ($51.4$\%) all possible
error messages in a mainstream \texttt{C++} compiler, and can provide at least
two distinct candidate patches for a large majority ($79.8$\%) of error
messages.  A majority ($59.6$\%) of the sythesized examples are ``small''
(a proxy for simplicity), containing five or fewer lines of code.  Lastly, in a
user study involving $14$ participants, with a median $2.5$ years of
\texttt{C++} experience, participants found the examples ``helpful'' in $5$ out
of $9$ tasks involving real-world \texttt{C++} compiler errors. 

In summary, our contributions are as follows:
\begin{itemize}
\item \textsc{Fuzz-and-Reduce}, an offline technique to generate and refine
compilation repair examples from a collection of compilable seed programs, and
an online technique to search and present relevant candiate repairs with examples
to users.

\item \textsc{CompAssist}, a practical realization of these techniques, and a
quantiative evaluation that shows that our approach can achieve high coverage
of compiler errors, in terms of breadth and depth, and that it can generate
simple examples. 

\item A user study that shows that \emph{synthetic} repair examples are helpful
to programmers in fixing compiler errors in real-world programs.

\end{itemize}


% contribution
% OFFLINE (do most work)
% 1. novel technique
% - mutation, small patches (separate issue)
% - reduce them
% ONLINE (do as little work as possible)
% 2. system present fix examples
% Evaluation


% Perspective
% Currently: online search with objective functions
% general methodology:
% applies to compilers and interpreters
% offline work (do as much work)
% logic errors with input and output test cases
% online work (do little work)

The remainder of the paper is organized as follows: In \autoref{sec:overview},
we provide an overview of \textsc{CompAssist}.  In \autoref{sec:offline} and
\autoref{sec:online} , we describe the offline generation and online search
components of our system.  In \autoref{sec:eval}, we present the results of
our quantitative evaluation.  In \autoref{sec:user-study}, we present our user
study.  In \autoref{sec:relwork}, we discuss related work.  Lastly, we
conclude.


