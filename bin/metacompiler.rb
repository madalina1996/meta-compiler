#!/usr/bin/env ruby

# This ruby script will call clang++-3.9 and pass it a file.
# Then it will parse the output of clang.
# It will try to get the diagnostic message produced by clang.
# It will search for that diagnostic message in our MEtacompiler database.
# It will return the url for that diagnostic messages which should contains examples
# that trigger that diagnostic message.

require 'open3'
require 'pg'
require 'cgi'
require 'base64'


VERSION = "1.0.1"
MC_URL = "https://srg.cs.ucdavis.edu/metacompiler"


def which(cmd)
  exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
  ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
    exts.each { |ext|
      exe = File.join(path, "#{cmd}#{ext}")
      return exe if File.executable?(exe) && !File.directory?(exe)
    }
  end
  return nil
end


def get_code(fname)
	return File.read(fname)
end


def get_fname(line)
	#puts line
	#puts "line.index(':')"
	#puts line.index(':')
	rindex = line.index(":") - 1
	#puts rindex
	return line.slice(0..rindex)
end


def get_link(err_line)
	err_id = 0
	# TODO: get error string
  err = err_line.slice(err_line.index("error:")..-1)
	#puts err.inspect                                                                 
  err = err.gsub('error: ','')                                                      
  #puts err.inspect                                                                 
  err = err.chomp                                                                   
  #puts err.inspect  
	url = "#{MC_URL}/compilers/1/diagnostic_messages/-1?m=#{CGI.escape(err)}"

	begin
		conn = PG::Connection.open( "postgres://metacompiler:metacompiler_2017@srg.cs.ucdavis.edu:5432/metacompiler_production" )
		conn.prepare('stmt1', "SELECT id FROM diagnostic_messages WHERE message=$1")
		rs = conn.exec_prepared('stmt1', [err]) 
		#puts rs.inspect
		if !rs.nil?
			#puts rs.values.inspect
			if !rs.values.first.nil?
				if !rs.values.first.first.nil?
					# get diag message id
					d_id = rs.values.first[0].to_i
					fname = get_fname(err_line)
					code = get_code(fname)
					code64 = Base64.encode64(code)
					url = "#{MC_URL}/compilers/1/diagnostic_messages/#{d_id}/embed.html"
					url = "#{url}?c=#{CGI.escape(code64)}"
				end
			end
		end

	rescue PG::Error => e
		puts e.message 
	ensure 
		rs.clear if rs
    conn.close if conn
	end

	return url
end


if __FILE__ == $0
	compiler = 'clang++-3.9'
	compiler = 'clang' if which('clang++-3.9').nil?
	#compiler = 'g++ -fmax-errors=1'
	compiler = "#{compiler} -ferror-limit=1"
	#compiler = 'clang++'
	#puts ARGV.inspect
	argv_str = ARGV.join(" ")
	#puts argv_str
	cmd = "#{compiler} #{argv_str}"

	#puts "#{compiler} #{argv_str}"
	out_str= ''
	Open3.popen2e(cmd) do |stdin,stdout_and_stderr,wait_thr|
		pid = wait_thr.pid # pid of started process	
		Thread.new do 
			while o = stdout_and_stderr.read
				out_str = out_str + o
			end
		end
		exit_stauts = wait_thr.value # Process::Status object returned
	end

	count = 0
	url = ''
	out_str.each_line do |line|
		if line =~ /error\:/ and count == 0
			url = get_link(line)
			count = count + 1
		end
		puts line
	end
	puts url
end # main

