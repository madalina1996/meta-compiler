package edu.ucdavis.error.fuzzer.core.engine;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CommandOptions;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.repo.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.mutator.ReplacementMutant;
import edu.ucdavis.error.fuzzer.token.IAtlasToken;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.util.*;

public class IdentifierSubstituionFuzzingEngine extends AbstractFuzzingEngine {

  private static final class IdentifierAndIndexPair {

    private final IAtlasToken identifier;

    private final int index;

    private final IAtlasToken replacingIdentifier;

    public IdentifierAndIndexPair(
        IAtlasToken identifier, int index, IAtlasToken replacingIdentifier) {
      super();
      this.identifier = identifier;
      this.index = index;
      this.replacingIdentifier = replacingIdentifier;
    }
  }

  private final ImmutableList<IdentifierAndIndexPair> useIdentifierList;

  private final Iterator<IdentifierAndIndexPair> mutationIterator;

  public IdentifierSubstituionFuzzingEngine(
      SourceFile seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager)
      throws ParsingFailureException, ExpansionOverlapsBoundaryException {
    super(seedFile, rootFolder, errorRepo, crashingBugPackager);
    this.useIdentifierList = this.buildAndShuffleUseIdentifierList();

    this.mutationIterator = useIdentifierList.iterator();
  }

  private ImmutableList<IdentifierAndIndexPair> buildAndShuffleUseIdentifierList() {
    final ArrayList<IdentifierAndIndexPair> list = new ArrayList<>();
    Map<String, IAtlasToken> identifierSet = new HashMap<>();

    for (int i = 0, size = this.originalProgram.size(); i < size; ++i) {
      final IAtlasToken token = this.originalProgram.get(i);
      throw new UnsupportedOperationException();
      //      if (token.isIdentifier() && token.isIdentifierUse()) {
      //        identifierSet.remove(token.getLexeme());
      //        for (IProgramSegment replacingIdentifier : identifierSet.values()) {
      //          list.add(new IdentifierAndIndexPair(token, i, replacingIdentifier));
      //        }
      //        identifierSet.put(token.getLexeme(), token);
      //      }
    }
    Collections.shuffle(list);
    return ImmutableList.copyOf(list);
  }

  // private static final int MUTANTS_UPPER_BOUND = 8000;
  private final int mutantsUpperBound = CommandOptions.v().getUpperBoundMutants();

  @Override
  protected boolean hasMoreMutants() {
    return this.getNumOfGeneratedMutants() < this.mutantsUpperBound
        && this.mutationIterator.hasNext();
  }

  @Override
  protected AbstractMutant internalMutate(AbstractMutant mutant) {
    // final ArrayList<IProgramSegment> mutant = new
    // ArrayList<>(this.originalProgram);

    final IdentifierAndIndexPair pair = this.mutationIterator.next();
    final IAtlasToken oldToken = pair.identifier;
    // mutant.set(pair.index, pair.replacingIdentifier);
    Logger.info("{}@{}-(replaced with)->{}", oldToken, pair.index, pair.replacingIdentifier);
    return new ReplacementMutant(this.originalProgram, pair.index, pair.replacingIdentifier);
  }
}
