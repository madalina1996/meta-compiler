

template <typename T> struct S {};

template <typename T> struct S<T *> {};

template <> struct S<int> {};

int main() {}
