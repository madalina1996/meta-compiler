namespace std {
template <class> struct char_traits;
template <typename> class allocator {
public:
  template <typename> struct rebind {};
};
} // namespace std
namespace __gnu_cxx {
template <typename, typename, typename> class __sso_string_base;
template <typename _CharT, typename = std ::char_traits<_CharT>,
          typename = std ::allocator<_CharT>,
          template <typename, typename, typename> class _Base =
              __sso_string_base>
class __versa_string;
template <typename _CharT, typename, typename _Alloc> struct __vstring_utility {
  typedef typename _Alloc ::template rebind<_CharT> _CharT_alloc_type;
};
template <typename _CharT, typename _Traits, typename _Alloc>
class __sso_string_base : __vstring_utility<_CharT, _Traits, _Alloc> {
  typedef __vstring_utility<_CharT, _Traits, _Alloc> _Util_Base;
  typename _Util_Base ::_CharT_alloc_type _CharT_alloc_type;
};
template <typename _CharT, typename _Traits, typename _Alloc,
          template <typename, typename, typename> class _Base>
class __versa_string : _Base<_CharT, _Traits, _Alloc> {};
} // namespace __gnu_cxx
template <typename _CharT, typename = std ::char_traits<_CharT>,
          typename = std ::allocator<_CharT>>
class basic_string : __gnu_cxx ::__versa_string<_CharT> {};
typedef basic_string<char> string;
class DL {
  struct {
    string capability;
  };
};
