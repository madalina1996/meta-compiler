package edu.ucdavis.error.fuzzer.token.cdt;

import com.google.common.base.Charsets;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.reducer.NonTokenPreservedTokenizer;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.google.common.io.Files.readLines;

/** Created by Chengnian Sun on 4/29/17. */
@RunWith(JUnit4.class)
public class CDTTokenizerTest {

  @Test
  public void testGreaterThanOperators()
      throws ExpansionOverlapsBoundaryException, ParsingFailureException, IOException {
    File parent = new File("test-data/" + getClass().getPackage().getName().replace('.', '/'));
    final File file = new File(parent, "complex_greater_than_operators.C");
    CDTTokenizer tokenizer = new CDTTokenizer(new SourceFile(file));
    ImmutableList<TokenProgramSegment> tokens = tokenizer.getTokens();
    final List<String> golden =
        readLines(
            new File(parent, "complex_greater_than_operator" + ".expected_token_list.txt"),
            Charsets.UTF_8);
    Truth.assertThat(FluentIterable.from(tokens).transform(TokenProgramSegment::getLexeme).toList())
        .isEqualTo(golden);
  }
}
