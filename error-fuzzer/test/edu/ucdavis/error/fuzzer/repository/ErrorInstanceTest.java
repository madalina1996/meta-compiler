package edu.ucdavis.error.fuzzer.repository;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static edu.ucdavis.error.fuzzer.repository.ErrorInstance.isGoodFile;

/** Created by Chengnian Sun on 5/2/17. */
@RunWith(JUnit4.class)
public class ErrorInstanceTest {

  private static void assertFalseMatching(List<String> fileNames) {
    fileNames.forEach(name -> assertThat(isGoodFile(new File(name))).isFalse());
  }

  private static void assertTrueMatching(List<String> fileNames) {
    fileNames.forEach(name -> assertThat(isGoodFile(new File(name))).isTrue());
  }

  @Test
  public void testIsGoodFile() {
    assertFalseMatching(ImmutableList.of("Good.c", "bad.c", "good.c.c", "good.", "good", "good.X"));
    assertTrueMatching(ImmutableList.of("good.c", "good.cc", "good.C", "good.cpp"));
  }
}
