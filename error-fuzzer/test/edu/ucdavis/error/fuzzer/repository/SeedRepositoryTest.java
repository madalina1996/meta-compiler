package edu.ucdavis.error.fuzzer.repository;

import com.google.common.truth.Truth;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

/**
 * Created by neo on 4/3/17.
 */
@RunWith(JUnit4.class)
public class SeedRepositoryTest {

  @Test
  public void testBuildRepo() {
    final File folder = new File("test-data/tiny-seed-repo");
    final SeedRepository repo = new SeedRepository.Builder(folder).build();
    Truth.assertThat(repo.getSeeds()).hasSize(4);
  }

  @Test
  public void testBuildRepoByDiscardingLargeFiles() {
    final File folder = new File("test-data/tiny-seed-repo");
    final SeedRepository repo = new SeedRepository.Builder(folder).discardLargeFiles(590961).build();
    Truth.assertThat(repo.getSeeds()).hasSize(3);
  }
  @Test
  public void testBuildRepoByDiscardingLargeFilesWith() {
    final File folder = new File("test-data/tiny-seed-repo");
    final SeedRepository repo = new SeedRepository.Builder(folder).discardLargeFiles(690962).build();
    Truth.assertThat(repo.getSeeds()).hasSize(4);
  }


}
