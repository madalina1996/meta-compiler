#!/usr/bin/env bash

set -o nounset
set -o pipefail

readonly CLANG_FORMAT="clang-format-3.8"
if ! command -v "${CLANG_FORMAT}" &> /dev/null ; then 
  echo "cannot locate ${CLANG_FORMAT} on the PATH"
  exit 1
fi



for file in $(find . -name "*.cpp") ; do
  $CLANG_FORMAT -style="{BasedOnStyle: llvm, ColumnLimit: 9999}" -i $file
  grep --with-filename --only-matching --extended-regexp "diag::[a-zA-Z][a-zA-Z0-9_]*" $file
done
