#!/usr/bin/env python2.7
#
# a wrapper for the clang compiler. As it compiles source code, it also 
# generates its LLVM IR representation (both .bc files and .ll files)
#
import sys
import os
import subprocess
import hook_compiler

exit(hook_compiler.run_wo_opt("gcc"))

