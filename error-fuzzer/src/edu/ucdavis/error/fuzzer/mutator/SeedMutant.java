package edu.ucdavis.error.fuzzer.mutator;

import java.util.ArrayList;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

public class SeedMutant extends AbstractMutant {

  protected final ImmutableList<IProgramSegment> seed;

  public SeedMutant(ImmutableList<IProgramSegment> seed) {
    super(null);
    this.seed = seed;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedSeed() {
    return new ArrayList<>(seed);
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedMutant() {
    return new ArrayList<>(seed);
  }

}
