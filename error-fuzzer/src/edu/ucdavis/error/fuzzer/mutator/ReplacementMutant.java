package edu.ucdavis.error.fuzzer.mutator;

import java.util.ArrayList;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

public class ReplacementMutant extends AbstractMutant {

  private final int index;

  private final IProgramSegment replacement;

  public ReplacementMutant(ImmutableList<IProgramSegment> seed, int index,
                           IProgramSegment replacement) {
    this(new SeedMutant(seed), index, replacement);
  }

  public ReplacementMutant(AbstractMutant chain, int index,
      IProgramSegment replacement) {
    super(chain);
    this.index = index;
    this.replacement = replacement;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedSeed() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedSeed();
    return mutant;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedMutant() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedMutant();
    mutant.set(this.index, this.replacement);
    return mutant;
  }

}
