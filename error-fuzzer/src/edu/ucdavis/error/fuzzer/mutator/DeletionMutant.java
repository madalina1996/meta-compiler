package edu.ucdavis.error.fuzzer.mutator;

import java.util.ArrayList;

import com.google.common.collect.ImmutableList;


import edu.ucdavis.error.fuzzer.token.segment.EmptyProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

public class DeletionMutant extends AbstractMutant {

  private final int index;

  public DeletionMutant(ImmutableList<IProgramSegment> seed, int index) {
    this(new SeedMutant(seed), index);
  }

  public DeletionMutant(AbstractMutant chain, int index) {
    super(chain);
    this.index = index;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedSeed() {
    return this.chain.getAlignedSeed();
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedMutant() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedMutant();
    mutant.set(this.index, EmptyProgramSegment.EMPTY_TOKEN);
    return mutant;
  }

}
