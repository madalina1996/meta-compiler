package edu.ucdavis.error.fuzzer.mutator;

/**
 * Created by neo on 4/30/17.
 *
 */
public enum EnumMutationType {

  DELETE,

  INSERT,

  SWAP,

  DUPLICATE,

  REPLACE;

}
