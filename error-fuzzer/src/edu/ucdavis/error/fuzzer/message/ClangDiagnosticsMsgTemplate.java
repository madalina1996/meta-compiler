package edu.ucdavis.error.fuzzer.message;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.pmw.tinylog.Logger;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import edu.ucdavis.error.fuzzer.core.FuzzingStatistics;

public class ClangDiagnosticsMsgTemplate {

  private static final String CATEGORY = "templates";

  private static final ClangSourcefileMessageMapping SOURCE_FILE_MSG_MAPPING = ClangSourcefileMessageMapping
      .getDeault();

  private final static Set<String> MSG_IDS_IN_FILES_OF_INTEREST = SOURCE_FILE_MSG_MAPPING
      .getMessagesInFilesOfInterest();

  private static final ImmutableList<String> PREFIXES_TO_FILTER = ImmutableList
      .of("pp", "note", "warn", "err_omp_", "err_objc",
          "not_conv_function_declared_at", "fatal_too_many_errors", "err_mmap",
          "err_imported", "error_objc_", "err_swift", "err_opencl",
          "err_module", "err_arc");

  private static List<ClangDiagnosticsMsgTemplate> loadTemplates(File file) {
    Logger.info("Loading error templates from file {}", file);
    List<ClangDiagnosticsMsgTemplate> templates = new ArrayList<ClangDiagnosticsMsgTemplate>();
    int rawTemplateCount = 0;
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      for (String line = reader.readLine(); line != null; line = reader
          .readLine()) {
        line = line.trim();
        if (line.isEmpty())
          continue;
        ++rawTemplateCount;
        templates.addAll(ClangDiagnosticsMsgTemplate.parseTemplate(line));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    FuzzingStatistics.v().setValue(CATEGORY, "raw_clang_msg_templates",
        Integer.toString(rawTemplateCount));

    Collections.sort(templates, (o1, o2) -> {
      final int l1 = o1.getPattern().pattern().length();
      final int l2 = o2.getPattern().pattern().length();
      if (l1 == l2) {
        return 0;
      } else if (l1 > l2) {
        return -1;
      } else {
        return 1;
      }
    });
    computeAndSaveStatistics(templates);
    return templates;
  }

  private static void computeAndSaveStatistics(
      List<ClangDiagnosticsMsgTemplate> templates) {
    Set<String> msgIds = new HashSet<>();
    templates.forEach(t -> msgIds.add(t.getId()));
    FuzzingStatistics stat = FuzzingStatistics.v();
    stat.setValue(CATEGORY, "distinct_msg_ids",
        Integer.toString(msgIds.size()));
    stat.setValue(CATEGORY, "error_regexp_templates",
        Integer.toString(templates.size()));
  }

  private static final List<ClangDiagnosticsMsgTemplate> TEMPLATES;

  public static void dumpTemplates(File file) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      for (ClangDiagnosticsMsgTemplate template : TEMPLATES) {
        writer.write(template.toString());
        writer.append("\n");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  static {
    TEMPLATES = loadTemplates(new File("all-clang-messages.txt"));
    dumpTemplates(new File("all_clang_template_regexp.txt"));
  }

  public static void main(String[] args) throws FileNotFoundException {
    final Multimap<String, String> errorToFiles = HashMultimap.create();
    for (ClangDiagnosticsMsgTemplate template : TEMPLATES) {
      template.getIncludingSourceFiles().forEach(file -> errorToFiles.put(template.getId(), file));
    }
    final PrintStream stream = new PrintStream(new File("error_id_2_source_file_map.txt"));
    for (Map.Entry<String, Collection<String>> entry : errorToFiles.asMap().entrySet()) {
      stream.println(entry.getKey());
      entry.getValue().forEach(value -> stream.println("    " + value));
    }
    stream.close();
  }

  public static class ErrorMsgTemplateMatchingFailureException
      extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private final String unhandledErrorMsg;

    public ErrorMsgTemplateMatchingFailureException(String unhandledErrorMsg) {
      super(
          "cannot find a template for the error message:" + unhandledErrorMsg);
      this.unhandledErrorMsg = unhandledErrorMsg;
    }

    public String getUnhandledErrorMsg() {
      return unhandledErrorMsg;
    }

  }

  public static List<ClangDiagnosticsMsgTemplate> getAllTemplates() {
    return TEMPLATES;
  }
  
  public static ClangDiagnosticsMsgTemplate matchTemplate(String errorMsg)
      throws ErrorMsgTemplateMatchingFailureException {
    for (ClangDiagnosticsMsgTemplate template : TEMPLATES) {
      if (template.getPattern().matcher(errorMsg).matches()) {
        return template;
      }
    }
    throw new ErrorMsgTemplateMatchingFailureException(errorMsg);
  }

  public static enum Severity {
    ERROR, FATAL, IGNORED, WARNING
  }

  private final String id;

  private final Severity severity;

  private final Pattern pattern;

  private final String originalClangMsgTemplate;

  private final Set<String> includingSourceFiles;

  private ClangDiagnosticsMsgTemplate(String id,
      String originalClangMsgTemplate, Severity severity, Pattern pattern,
      Set<String> includingSourceFiles) {
    super();
    this.id = id;
    this.originalClangMsgTemplate = originalClangMsgTemplate;
    this.severity = severity;
    this.pattern = pattern;
    this.includingSourceFiles = includingSourceFiles;
  }

  public String getOriginalClangMsgTemplate() {
    return originalClangMsgTemplate;
  }

  /**
   * return the source files which can emit this error message.
   * 
   * @return
   */
  public Iterable<String> getIncludingSourceFiles() {
    return includingSourceFiles;
  }

  /**
   * false if removing this template.
   * 
   * @param id
   * @param severity
   * @return
   */
  private static boolean filterTemplate(String id, Severity severity,
      String message) {
    if (severity != Severity.ERROR && severity != Severity.FATAL) {
      return true;
    }
    for (String prefix : PREFIXES_TO_FILTER) {
      if (id.startsWith(prefix)) {
        return true;
      }
    }
    if (!MSG_IDS_IN_FILES_OF_INTEREST.contains(id)) {
      return true;
    }
    return false;
  }

  public static List<ClangDiagnosticsMsgTemplate> parseTemplate(String line) {
    // warn_doc_param_spaces_in_direction (unsigned)diag::Severity::Ignored
    // "whitespace is not allowed in parameter passing direction"
    final String[] segments = line.split("\t");
    Preconditions.checkState(segments.length == 3);

    final String id = segments[0].trim();
    final Severity severity = parseSeverity(segments[1].trim());
    String template = segments[2].trim();

    if (filterTemplate(id, severity, template)) {
      return Collections.emptyList();
    }

    assert template.charAt(0) == '"';
    assert template.charAt(template.length() - 1) == '"';
    final List<Pattern> patterns = new ClangErrorMsgTemplateParser(
        template.substring(1, template.length() - 1)).getPatterns();
    final List<ClangDiagnosticsMsgTemplate> templates = new ArrayList<>();
    for (Pattern pattern : patterns) {
      if (pattern.pattern().equals(".+") || pattern.pattern().equals(".*")
          || pattern.pattern().equals(".?")) {
        continue;
      }
      templates.add(new ClangDiagnosticsMsgTemplate(id, line, severity, pattern,
          SOURCE_FILE_MSG_MAPPING.getFilesUsingTheMessage(id)));
    }
    return templates;
  }

  public String getId() {
    return id;
  }

  public Severity getSeverity() {
    return severity;
  }

  public Pattern getPattern() {
    return pattern;
  }

  private static Severity parseSeverity(String string) {
    switch (string) {
    case "(unsigned)diag::Severity::Error":
      return Severity.ERROR;
    case "(unsigned)diag::Severity::Fatal":
      return Severity.FATAL;
    case "(unsigned)diag::Severity::Ignored":
      return Severity.IGNORED;
    case "(unsigned)diag::Severity::Warning":
      return Severity.WARNING;
    default:
      throw new RuntimeException("Unhandled severity " + string);
    }
  }

  @Override
  public String toString() {
    return "ClangDiagnosticsMsgTemplate [id=" + id + ", severity=" + severity
        + ", pattern=" + pattern + "]";
  }

}