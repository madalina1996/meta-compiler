package edu.ucdavis.error.fuzzer.compiler;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class SourceFile {

  public static final ImmutableList<String> CPP_SOURCE_FILE_EXTENTIONS =
      ImmutableList.of(".C", ".cc", ".cpp", ".CC", "cxx");

  public static boolean isCppSourceFile(String fileName) {
    return CPP_SOURCE_FILE_EXTENTIONS
            .stream()
            .filter(ext -> fileName.endsWith(ext))
            .count()
        > 0;
  }

  public static boolean isCSourceFile(String fileName) {
    return fileName.endsWith(".c");
  }

  public static boolean isSourceFile(String fileName) {
    return isCSourceFile(fileName) || isCppSourceFile(fileName);
  }

  public static boolean isSourceFile(File file) {
    return isSourceFile(file.getName());
  }

  private final File file;

  private final boolean isCppFile;

  private final String fileNameExtension;

  public SourceFile(File file) {
    this.file = file.getAbsoluteFile();
    final String fileName = file.getName();
    this.isCppFile = isCppSourceFile(fileName);
    this.fileNameExtension = "." + FilenameUtils.getExtension(fileName);
  }

  public String getFileNameExtension() {
    return fileNameExtension;
  }

  public String getFileName() {
    return this.file.getName();
  }

  public File getAbsoluteFile() {
    return file;
  }

  public boolean isCppFile() {
    return isCppFile;
  }

  @Override
  public String toString() {
    return this.file.toString();
  }
}
