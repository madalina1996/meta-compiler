package edu.ucdavis.error.fuzzer.compiler;

public class CompilerTestConfigFactory {

  private static String computeStdFlag(boolean isCpp) {
    return "-std=" + (isCpp ? "c++14" : "c11");
  }

  public static AbstractCompilerTestConfig createGccConfig(boolean isCpp,
      String compilerSuffix, boolean enableAllWarningFlags,
      boolean enableFatalErrors, String flags) {
    final String compiler = (isCpp ? "g++" : "gcc") + compilerSuffix;
    final String std = computeStdFlag(isCpp);
    return new GccCompilerTestConfig(compiler, isCpp, enableAllWarningFlags,
        enableFatalErrors, flags + " " + std);
  }

  public static AbstractCompilerTestConfig createClangConfig(boolean isCpp,
      String compilerSuffix, boolean enableAllWarningFlags,
      boolean enableFatalErrors, String flags) {
    final String compiler = (isCpp ? "clang++" : "clang") + compilerSuffix;
    return new ClangCompilerTestConfig(compiler, isCpp, enableAllWarningFlags,
        enableFatalErrors, flags + " " + computeStdFlag(isCpp));
  }

  public static AbstractCompilerTestConfig createCompCertConfig() {
    return new CompCertCompilerTestConfig("ccomp-trunk", "-fall");
  }

  private static class ClangCompilerTestConfig
      extends AbstractCompilerTestConfig {

    protected ClangCompilerTestConfig(String compiler, boolean isCppCompiler,
        boolean enableAllWarningFlags, boolean enableFatalErrors,
        String flags) {
      super(compiler, isCppCompiler, enableAllWarningFlags, enableFatalErrors,
          flags);
    }

    @Override
    protected String getDisableWarningFlags() {
      return "-w";
    }

    @Override
    protected String getAllWarningFlags() {
      return "-Weverything";
    }

    @Override
    public boolean doesCompilerCrash(String compilerOutput) {
      return compilerOutput.contains("PLEASE");
    }

    @Override
    protected String getFatalErrorFlag() {
      return "-Wfatal-errors";
    }

  }

  private static class GccCompilerTestConfig
      extends AbstractCompilerTestConfig {

    protected GccCompilerTestConfig(String compiler, boolean isCppCompiler,
        boolean enableAllWarningFlags, boolean enableFatalErrors,
        String flags) {
      super(compiler, isCppCompiler, enableAllWarningFlags, enableFatalErrors,
          flags);
    }

    @Override
    protected String getAllWarningFlags() {
      return "-Wall -Wextra";
    }

    @Override
    protected String getFatalErrorFlag() {
      return "-Wfatal-errors";
    }

    @Override
    public boolean doesCompilerCrash(String compilerOutput) {
      return compilerOutput.contains("internal compiler error");
    }

    @Override
    protected String getDisableWarningFlags() {
      return "-w";
    }

  }

  private static class CompCertCompilerTestConfig
      extends AbstractCompilerTestConfig {

    protected CompCertCompilerTestConfig(String compiler, String flags) {
      super(compiler, false, false, false, flags);
    }

    @Override
    protected String getDisableWarningFlags() {
      return "";
    }

    @Override
    protected String getFatalErrorFlag() {
      return "";
    }

    @Override
    protected String getAllWarningFlags() {
      return "";
    }

    @Override
    public boolean doesCompilerCrash(String compilerOutput) {
      return compilerOutput.contains("Fatal error: ");
    }

  }

}
