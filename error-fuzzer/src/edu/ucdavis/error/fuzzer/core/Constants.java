package edu.ucdavis.error.fuzzer.core;

import com.google.common.base.Charsets;

import java.nio.charset.Charset;

/**
 * Created by Chengnian Sun on 3/28/17.
 */
public interface Constants {

  Charset DEFAULT_CHARSET = Charsets.UTF_8;

}
