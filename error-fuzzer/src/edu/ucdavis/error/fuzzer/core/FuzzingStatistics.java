package edu.ucdavis.error.fuzzer.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import com.google.common.base.Throwables;

public class FuzzingStatistics {

  private final static FuzzingStatistics DEFAULT = new FuzzingStatistics(
      new File("statistics.properties"));

  public static FuzzingStatistics v() {
    return DEFAULT;
  }

  private final File file;

  private final java.util.Properties properties;

  private void loadFromFile() throws IOException {
    if (!file.exists() || !file.isFile()) {
      return;
    }
    try (FileInputStream is = new FileInputStream(file)) {
      properties.load(is);
    }
  }

  public FuzzingStatistics(File file) {
    super();
    this.file = file;
    this.properties = new Properties() {

      private static final long serialVersionUID = 1L;

      public synchronized java.util.Enumeration<Object> keys() {
        Enumeration<Object> keysEnum = super.keys();
        Vector<Object> keyList = new Vector<Object>();

        while (keysEnum.hasMoreElements()) {
          keyList.add(keysEnum.nextElement());
        }

        Collections.sort(keyList,
            (o1, o2) -> o1.toString().compareTo(o2.toString()));
        return keyList.elements();
      };
    };
    try {
      loadFromFile();
    } catch (IOException e) {
      throw new ErrorFuzzerFatalError(e);
    }
  }

  public synchronized void setValue(String category, String key, String value) {
    if (category == null) {
      category = "";
    } else {
      category += ".";
    }
    this.properties.put(category + key, value);
    try {
      saveToFile();
    } catch (IOException e) {
      throw Throwables.propagate(e);
    }
  }

  private void saveToFile() throws IOException {
    final File parent = file.getParentFile();
    if (parent != null && !parent.exists()) {
      parent.mkdirs();
    }
    try (FileOutputStream os = new FileOutputStream(file)) {
      properties.store(os, "updated on " + new Date());
    }
  }

}
