package edu.ucdavis.error.fuzzer.core;

import edu.ucdavis.error.fuzzer.core.engine.FuzzingEngineFactory;
import edu.ucdavis.error.fuzzer.core.engine.RandomTokenManipulationFuzzingEngine;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.util.RandomUtil;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FuzzingLoop {

  public static FuzzingLoop createFuzzingLoop(
      File rootFolder,
      TokenizedProgramRepository tokenizedProgramRepository,
      Class<? extends AbstractFuzzingEngine> engineClass,
      int maxInstancesPerErrorMsg) {
    Logger.info("Fuzzing engine type is {}", engineClass);
    final CompilerErrorRepository errorRepo =
        CompilerErrorRepository.createOrLoadErrorRepository(
            new File(rootFolder, "error/" + engineClass.getSimpleName()), maxInstancesPerErrorMsg);
    final CrashingBugPackager crashingBugPackager =
        new CrashingBugPackager(new File(rootFolder, "crash/" + engineClass.getSimpleName()));

    final FuzzingEngineFactory engineFactory = new FuzzingEngineFactory(engineClass);
    return new FuzzingLoop(
        rootFolder, errorRepo, crashingBugPackager, tokenizedProgramRepository, engineFactory);
  }

  private final File rootFolder;
  private final TokenizedProgramRepository tokenizedProgramRepository;
  private final CompilerErrorRepository errorRepo;
  private final CrashingBugPackager crashingBugPackager;
  private final FuzzingEngineFactory factory;

  private final List<AbstractFuzzingEngine> fuzzingTasks;

  private FuzzingLoop(
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager,
      TokenizedProgramRepository tokenizedProgramRepository,
      FuzzingEngineFactory factory) {
    this.rootFolder = rootFolder;
    this.tokenizedProgramRepository = tokenizedProgramRepository;
    this.errorRepo = errorRepo;
    this.crashingBugPackager = crashingBugPackager;
    this.factory = factory;
    this.fuzzingTasks = new ArrayList<>();
    for (SourceProgramWithTokens program : tokenizedProgramRepository.getPrograms()) {
      try {
        final AbstractFuzzingEngine engine =
            new RandomTokenManipulationFuzzingEngine(
                program, rootFolder, errorRepo, crashingBugPackager);
        if (!engine.preRunCheck()) {
          Logger.info("The preRunCheck failed for source file {}", program.getSourceFile());
          continue;
        }
        this.fuzzingTasks.add(engine);
      } catch (Throwable e) {
        throw new AssertionError("Exception is not expected here. ", e);
      }
    }
    Logger.info("Created {} fuzzing engines", fuzzingTasks.size());
  }

  public void run() {
    while (true) {
      for (AbstractFuzzingEngine engine : this.fuzzingTasks) {
        final int iterations = 10 * (1 + RandomUtil.v().nextInt(10));
        Logger.info("Run mutation engine for {} iterations", iterations);
        engine.run(iterations);
      }
    }
  }
}
