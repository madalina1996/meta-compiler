package edu.ucdavis.error.fuzzer.core;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.AbstractCompilerTestConfig;
import edu.ucdavis.error.fuzzer.compiler.CompilerTestConfigFactory;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.Shell.CmdOutput;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.ErrorInstance;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.message.ClangDiagnosticsMsgTemplate.ErrorMsgTemplateMatchingFailureException;
import edu.ucdavis.error.fuzzer.message.CompilerError;
import edu.ucdavis.error.fuzzer.message.ErrorParser;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.mutator.SeedMutant;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.util.PathEnvUtil;
import edu.ucdavis.error.fuzzer.util.RandomUtil;
import edu.ucdavis.error.fuzzer.util.TimeoutUtil;
import edu.ucdavis.error.fuzzer.reducer.ErrorReducer;
import org.apache.commons.io.FileUtils;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractFuzzingEngine {

  private final SourceProgramWithTokens seedFile;
  private final CrashingBugPackager crashingBugPackager;
  private final CompilerErrorRepository errorRepo;
  private final File workingFolder;
  private final File mutantFile;
  private final AbstractCompilerTestConfig primaryCompiler;
  private final ImmutableList<AbstractCompilerTestConfig> secondaryCompilers;
  private final File tmpOutputFile;
  protected final RandomUtil random;
  private final List<Integer> iterationsThatFoundNewErrors; // iterations that found new errors.

  private CmdOutput compileProgram(AbstractCompilerTestConfig compiler, File sourceFile) {
    return compileProgram(compiler, sourceFile, this.tmpOutputFile);
  }

  private static boolean isTimeoutEnabled() {
    return CommandOptions.v().isTimeoutEnabled();
  }

  private static boolean isTestingCompCert() {
    return CommandOptions.v().testingCompCert();
  }

  private static boolean isTestingGccTrunk() {
    return CommandOptions.v().testingGccForCrashes();
  }

  private static boolean isTestingClangTrunk() {
    return CommandOptions.v().testingClangForCrashes();
  }

  private static String getTimeoutCmd() {
    return TimeoutUtil.getTimeoutCmd();
  }

  private static String getTimeoutCmdIfEnabled() {
    final CommandOptions options = CommandOptions.v();
    if (!options.isTimeoutEnabled()) {
      return "";
    }
    return TimeoutUtil.getTimeoutCmd() + " -s 9 " + options.getTimeoutThreshold() + " ";
  }

  private CmdOutput compileProgram(
      AbstractCompilerTestConfig compiler, File sourceFile, File outputFile) {
    return Shell.run(
        getTimeoutCmdIfEnabled()
            + compiler.getCompilerCmd()
            + sourceFile.getAbsolutePath()
            + " -o "
            + outputFile.getAbsolutePath());
  }

  //  private static void ensureFolderExists(File folder) {
  //    if (!folder.exists()) {
  //      folder.mkdirs();
  //    }
  //  }

  public static AbstractCompilerTestConfig getPrimaryCompilerConfig(boolean isCppProgram) {
    return CompilerTestConfigFactory.createClangConfig(
        isCppProgram, "-trunk", true, true, " -O0 " + "" + "-c");
  }

  public AbstractFuzzingEngine(
      final SourceProgramWithTokens seedFile,
      final File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager)
      throws ParsingFailureException {
    this.random = RandomUtil.v();
    this.seedFile = seedFile;
    this.errorRepo = errorRepo;

    final File engineRootFolder =
        new File(rootFolder, this.getClass().getSimpleName()).getAbsoluteFile();
    this.crashingBugPackager = crashingBugPackager;
    this.workingFolder = new File(engineRootFolder, "tmp_TID-" + Thread.currentThread().getId());
    this.tmpOutputFile = new File(this.workingFolder, "temp.o").getAbsoluteFile();

    this.crashingBugPackager.getCrashingBugFolder().mkdirs();
    this.workingFolder.mkdirs();

    boolean isCppProgram = this.seedFile.getSourceFile().isCppFile();
    this.primaryCompiler = getPrimaryCompilerConfig(isCppProgram);

    this.secondaryCompilers = buildSecondaryCompilers(isCppProgram);

    this.mutantFile =
        new File(
            this.workingFolder, "mutant" + this.seedFile.getSourceFile().getFileNameExtension());

    this.checkCompilerExistence();

    this.iterationsThatFoundNewErrors = new ArrayList<>();
  }

  private void checkCommandExistence(String cmd) {
    if (!PathEnvUtil.existCommand(cmd)) {
      throw new ErrorFuzzerFatalError("cannot find command " + cmd + " on the PATH.");
    }
  }

  private void checkCompilerExistence(AbstractCompilerTestConfig compiler) {
    checkCommandExistence(compiler.getCompiler());
  }

  private void checkCompilerExistence() {
    checkCompilerExistence(this.primaryCompiler);
    for (AbstractCompilerTestConfig compiler : this.secondaryCompilers) {
      checkCompilerExistence(compiler);
    }
    if (isTimeoutEnabled()) {
      checkCommandExistence(getTimeoutCmd());
    }
  }

  private static ImmutableList<AbstractCompilerTestConfig> buildSecondaryCompilers(
      boolean isCppProgram) {
    final ImmutableList.Builder<AbstractCompilerTestConfig> builder = ImmutableList.builder();
    if (isTestingClangTrunk()) {
      builder.add(
          CompilerTestConfigFactory.createClangConfig(
              isCppProgram, "-trunk", true, false, "-O3 -c"));
    }

    if (isTestingGccTrunk()) {
      builder.add(
          CompilerTestConfigFactory.createGccConfig(isCppProgram, "-trunk", true, false, "-O3 -c"));
    }

    if (!isCppProgram && isTestingCompCert()) {
      builder.add(CompilerTestConfigFactory.createCompCertConfig());
    }
    return builder.build();
  }

  private boolean isCrashingBugTriggered(AbstractCompilerTestConfig compiler, CmdOutput output) {
    if (output.getExitCode() == 0) return false;
    return compiler.doesCompilerCrash(output.getStderr());
  }

  private boolean isPerformanceBugTriggered(AbstractCompilerTestConfig compiler, CmdOutput output) {
    return output.getExitCode() == 137;
  }

  private void saveCrashingBug(
      AbstractCompilerTestConfig compilerConfig, CmdOutput crashingOutput) {
    this.crashingBugPackager.packageCrashingBug(
        this.seedFile.getSourceFile(), this.mutantFile, compilerConfig, crashingOutput);
  }

  private void savePerformanceBug(
      AbstractCompilerTestConfig compilerConfig, CmdOutput compilerOutput) {
    this.crashingBugPackager.packagePerformanceBug(
        this.seedFile.getSourceFile(), this.mutantFile, compilerConfig, compilerOutput);
  }

  /**
   * @param mutant
   * @param compilerError
   * @return {@code true} if a new error message is found.
   */
  private boolean saveErrorMsgAndFiles(AbstractMutant mutant, CompilerError compilerError) {
    Optional<ErrorInstance> instance =
        this.errorRepo.saveErrorInstance(
            mutant, compilerError, this.seedFile.getSourceFile(), this.mutantFile);
//    instance.ifPresent(
//        ei -> {
////          try {
//            new ErrorReducer(ei).reduce();
////          } catch (Throwable e) {
////            final File folder = ei.getFolder();
////            Logger.error("There is an error in reducing the error instance {}", folder);
////            final File newName =
////                new File(folder.getParentFile(), "reduction-error-" + folder.getName());
////            Logger.error("rename the folder {} to {}", folder, newName);
////            folder.renameTo(newName);
////          }
//        });
    return instance.isPresent();
  }

  public boolean compileWithPrimary(AbstractMutant mutant) {
    final CmdOutput output = this.compileProgram(this.primaryCompiler, this.mutantFile);
    if (output.getExitCode() == 0) {
      return false;
    }
    if (this.isPerformanceBugTriggered(this.primaryCompiler, output)
        || this.isCrashingBugTriggered(this.primaryCompiler, output)) {
      // if there is a compiler bug, then skip it now.
      return false;
    }
    assert (output.getExitCode() != 0);
    try {
      final CompilerError compilerError = ErrorParser.parseFirstError(output.getStderr());
      if (compilerError != null) {
        return saveErrorMsgAndFiles(mutant, compilerError);
      }
      return false;
    } catch (ErrorMsgTemplateMatchingFailureException e) {
      // Logger.error(e);
      final String unhandledErrorMsg = e.getUnhandledErrorMsg();
      Logger.warn("find an unhandled error msg {}", unhandledErrorMsg);
      this.errorRepo.saveUnhandledErrorMessage(unhandledErrorMsg);
      return false;
    }
  }

  private void compileWithSecondary() {
    for (AbstractCompilerTestConfig compiler : this.secondaryCompilers) {
      final CmdOutput output = this.compileProgram(compiler, this.mutantFile);
      if (this.isCrashingBugTriggered(compiler, output)) {
        this.saveCrashingBug(compiler, output);
      } else if (this.isPerformanceBugTriggered(compiler, output)) {
        this.savePerformanceBug(compiler, output);
      }
    }
  }

  public boolean preRunCheck() {
    if (this.compileProgram(this.primaryCompiler, this.seedFile.getSourceFile().getAbsoluteFile())
            .getExitCode()
        != 0) {
      Logger.error(
          "The seed program {} cannot compile", this.seedFile.getSourceFile().getFileName());
      return false;
    }
    return true;
  }

  /**
   * @param iterations
   * @return {@code true} if a new error message is found.
   */
  public final boolean run(int iterations) {
    Preconditions.checkArgument(iterations > 0);
    final ImmutableList<IProgramSegment> tokens = ImmutableList.copyOf(this.seedFile.getTokenList());
    Logger.info(
        "Mutating {}, Engine={}, iterations={}",
        this.seedFile.getSourceFile().getFileName(),
        this.getClass().getSimpleName(), iterations);
    boolean result= false;
    for (int i = 0; i < iterations;) {
      AbstractMutant mutant = new SeedMutant(tokens);
      //AbstractMutant mutant = this.mutate(tokens);
      for (int j = 0; j < 3 && i < iterations; ++j) {
        ++i;
        try {
          FileUtils.writeStringToFile(
              this.mutantFile, mutant.getAlignedMutantAsString(), Charsets.UTF_8, false);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        final boolean foundNewError = this.compileWithPrimary(mutant);
        if (foundNewError) {
          this.iterationsThatFoundNewErrors.add(this.numOfMutants);
        }
        result = result || foundNewError;

        this.compileWithSecondary();
        mutant = this.mutate(mutant);
        ++i;
      }
    }
    return result;
  }

  private int numOfMutants;

  public int getNumOfGeneratedMutants() {
    return this.numOfMutants;
  }

  protected abstract boolean hasMoreMutants();

  protected final AbstractMutant mutate(AbstractMutant mutant) {
    ++numOfMutants;
    return this.internalMutate(mutant);
  }

  protected abstract AbstractMutant internalMutate(AbstractMutant mutant);
}
