package edu.ucdavis.error.fuzzer.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.pmw.tinylog.Logger;

import com.google.common.base.Throwables;

public class Shell {

  public static class CmdOutput {

    private final int exitCode;

    private final String stdout;

    private final String stderr;

    public CmdOutput(int exitCode, String stdout, String stderr) {
      super();
      this.exitCode = exitCode;
      this.stdout = stdout;
      this.stderr = stderr;
    }

    public int getExitCode() {
      return exitCode;
    }

    public String getStdout() {
      return stdout;
    }

    public String getStderr() {
      return stderr;
    }

    @Override
    public String toString() {
      return "CmdOutput [exitCode=" + exitCode + ", stdout=" + stdout
          + ", stderr=" + stderr + "]";
    }

  }

  public static CmdOutput run(String cmd) {
    CommandLine commandline = CommandLine.parse(cmd);
    
    return run(commandline);
  }

  public static CmdOutput run(String cmd, String[] arguments) {
    CommandLine commandline = new CommandLine(cmd);
    commandline.addArguments(arguments);
    return run(commandline);
  }
  
  public static CmdOutput run(CommandLine commandline) {
    ByteArrayOutputStream stdout = new ByteArrayOutputStream();
    ByteArrayOutputStream stderr = new ByteArrayOutputStream();

    DefaultExecutor exec = new DefaultExecutor();
    PumpStreamHandler streamHandler = new PumpStreamHandler(stdout, stderr);
    exec.setStreamHandler(streamHandler);
    int exitCode;
    try {
      Logger.debug("{}", commandline);
      exitCode = exec.execute(commandline);
    } catch (ExecuteException e) {
      Logger.trace("error when running cmd {}", commandline.toString());
      Logger.trace("cmd stdout {}", stdout);
      Logger.trace("cmd stderr {}", stderr);
      exitCode = e.getExitValue();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return new CmdOutput(exitCode, stdout.toString(), stderr.toString());
  }

}
