package edu.ucdavis.error.fuzzer.core.engine;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.CommandOptions;
import edu.ucdavis.error.fuzzer.core.CrashingBugPackager;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.TokenizedProgramRepository.SourceProgramWithTokens;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.mutator.DeletionMutant;
import edu.ucdavis.error.fuzzer.mutator.InsertionMutant;
import edu.ucdavis.error.fuzzer.mutator.ReplacementMutant;
import edu.ucdavis.error.fuzzer.mutator.SwapMutant;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;

public class RandomTokenManipulationFuzzingEngine extends AbstractFuzzingEngine {

  public RandomTokenManipulationFuzzingEngine(
      SourceProgramWithTokens seedFile,
      File rootFolder,
      CompilerErrorRepository errorRepo,
      CrashingBugPackager crashingBugPackager)
      throws IOException, ParsingFailureException {
    super(seedFile, rootFolder, errorRepo, crashingBugPackager);
  }

  private final int mutantsUpperBound = CommandOptions.v().getUpperBoundMutants();

  @Override
  protected boolean hasMoreMutants() {
    throw new UnsupportedOperationException("Not implemented yet");
  }

  private AbstractMutant mutateBySubstitution(AbstractMutant mutant) {
    final ArrayList<IProgramSegment> tokens = mutant.getAlignedMutant();
    final int tokenCount = tokens.size();
    final int sourceIndex = this.random.nextInt(tokenCount);
    int targetIndex;
    for (targetIndex = this.random.nextInt(tokenCount);
        targetIndex != sourceIndex;
        targetIndex = this.random.nextInt(tokenCount)) {}
    return new ReplacementMutant(mutant, targetIndex, tokens.get(sourceIndex));
  }

  private AbstractMutant mutateBySwapping(AbstractMutant mutant) {
    final int tokenCount = mutant.getAlignedMutant().size();
    final int sourceIndex = this.random.nextInt(tokenCount);
    int targetIndex;
    for (targetIndex = this.random.nextInt(tokenCount);
        targetIndex != sourceIndex;
        targetIndex = this.random.nextInt(tokenCount)) {}
    return new SwapMutant(mutant, targetIndex, sourceIndex);
  }

  private AbstractMutant mutateByDeletion(AbstractMutant mutant) {
    return new DeletionMutant(mutant, this.random.nextInt(mutant.getAlignedMutant().size()));
  }

  private AbstractMutant mutateByInsertion(AbstractMutant mutant) {
    final ArrayList<IProgramSegment> tokens = mutant.getAlignedMutant();
    assert (tokens.size() > 1);

    int sourceIndex;
    int targetIndex;
    do {
      sourceIndex = this.random.nextInt(tokens.size());
      targetIndex = this.random.nextInt(tokens.size() + 1);
    } while (sourceIndex == targetIndex);

    return new InsertionMutant(mutant, targetIndex, tokens.get(sourceIndex));
  }

  private AbstractMutant mutateByDuplication(AbstractMutant mutant) {
    final ArrayList<IProgramSegment> tokens = mutant.getAlignedMutant();
    assert (tokens.size() > 1);

    int sourceIndex;
    int targetIndex;
    int targetIndex2;
    do {
      sourceIndex = this.random.nextInt(tokens.size());
      targetIndex = sourceIndex + 1;
      targetIndex2 = this.random.nextInt(tokens.size() + 1);
    } while (sourceIndex == targetIndex2);

    return new InsertionMutant(mutant, targetIndex, tokens.get(sourceIndex));
  }

  @Override
  protected AbstractMutant internalMutate(AbstractMutant mutant) {
    // final ArrayList<IProgramSegment> mutant = new
    // ArrayList<>(this.originalProgram);
    final int choice = this.random.nextInt(5);
    switch(choice) {
      case 0:
        return this.mutateBySubstitution(mutant);
      case 1:
        return this.mutateByDeletion(mutant);
      case 2:
        return this.mutateByInsertion(mutant);
      case 3:
        return this.mutateBySwapping(mutant);
      case 4:
        return this.mutateByDuplication(mutant);
      default:
        throw new AssertionError("Cannot reach here. ");
    }
  }
}
