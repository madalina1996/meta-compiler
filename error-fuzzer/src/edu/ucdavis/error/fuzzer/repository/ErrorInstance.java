package edu.ucdavis.error.fuzzer.repository;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.message.CompilerError;
import edu.ucdavis.error.fuzzer.mutator.AbstractMutant;
import edu.ucdavis.error.fuzzer.token.TokenUtility;
import edu.ucdavis.error.fuzzer.util.FileUtility;
import org.apache.commons.io.FileUtils;
import org.pmw.tinylog.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;

/** Created by neo on 2/20/17. */
public class ErrorInstance {

  public static ErrorInstance load(File instanceFolder) {
    final SourceFile goodFile = new SourceFile(locateGoodFile(instanceFolder));
    final ErrorInstance instance =
        new ErrorInstance(instanceFolder, goodFile.getFileNameExtension());
    final IntegrityReport problems = instance.checkIntegrity();
    problems.logProblems();
    return instance;
  }

  public static ErrorInstance create(
      File instanceFolder,
      AbstractMutant mutant,
      CompilerError compilerError,
      SourceFile seedFile,
      File mutantFile) {
    final ErrorInstance instance =
        new ErrorInstance(instanceFolder, seedFile.getFileNameExtension());

    try {
      FileUtils.copyFile(seedFile.getAbsoluteFile(), instance.goodFile);
      FileUtils.write(
          instance.goodTokenTypeFile,
          TokenUtility.s().convertSegmentTypeToString(mutant.getAlignedSeed()),
          Charsets.UTF_8);
      FileUtils.write(
          instance.goodTokenLexemeFile,
          TokenUtility.s().convertTokenSequenceToProgram(mutant.getAlignedSeed()),
          Charsets.UTF_8);

      FileUtils.copyFile(mutantFile, instance.badFile);
      FileUtils.write(
          instance.badTokenTypeFile,
          TokenUtility.s().convertSegmentTypeToString(mutant.getAlignedMutant()),
          Charsets.UTF_8);
      FileUtils.write(
          instance.badTokenLexemeFile,
          TokenUtility.s().convertTokenSequenceToProgram(mutant.getAlignedMutant()),
          Charsets.UTF_8);

      FileUtils.write(instance.msgFile, compilerError.getMessage() + "\n", Charsets.UTF_8);
      FileUtils.write(
          instance.errorIDFile, compilerError.getTemplate().getId() + "\n", Charsets.UTF_8);
      FileUtils.write(
          instance.clangErrorTemplateFile,
          compilerError.getTemplate().getOriginalClangMsgTemplate() + "\n",
          Charsets.UTF_8);
      FileUtils.write(instance.reductionSeeFile, mutant.getAlignedSeedAsString(), Charsets.UTF_8);
      FileUtils.write(
          instance.reductionMutantFile, mutant.getAlignedMutantAsString(), Charsets.UTF_8);
    } catch (IOException e) {
      Logger.error(e);
    }

    return instance;
  }

  public File getFolder() {
    return folder;
  }

  /** the folder whether this instance is stored. */
  private final File folder;

  /** good.c, the seed file that passes the compilation */
  private final File goodFile;

  /** good.token.lexeme.txt, the lexemes for {@code goodFile} */
  private final File goodTokenLexemeFile;

  /** good.token.type.txt, the token types for {@code goodFile} */
  private final File goodTokenTypeFile;

  /** stores the content of the goodFile, with whitespaces ignored. */
  private String goodFileContentWithNoWhitespaces;

  /** bad.c, the mutant that fails the compilation */
  private final File badFile;

  /** bad.token.lexeme.txt, the lexemes for {@code badFile} */
  private final File badTokenLexemeFile;

  /** bad.token.type.txt, the token types for {@code badFile} */
  private final File badTokenTypeFile;

  /** the file that stores the concrete compiler error message */
  private final File msgFile;

  /** the file that stores the error message ID */
  private final File errorIDFile;

  /** the file that stores the signature of the mutator that produces the current error instance. */
//  private final File mutatorSignature;

  /** the file that stores the clange error message template. */
  private final File clangErrorTemplateFile;

  /** the reduction folder, for reductino purpose. */
  private final File reductionFolder;

  /** the seed file for reduction */
  private final File reductionSeeFile;

  /** the mutant file for reduction. */
  private final File reductionMutantFile;

  /** the reduced good (seed) program, emitted line by line */
  private final File reducedLinedGoodFile;

  /** the reduced bad (mutant) program, emitted line by line */
  private final File reducedLinedBadFile;

  /** the reduced, formatted good (seed) program, emitted line by line */
  private final File reducedFormattedGoodFile;

  /** the reduced, formatted bad (seed) program, emitted line by line */
  private final File reducedFormattedBadFile;

  private ErrorInstance(final File folder, final String extName) {
    this.folder = folder;
    if (!folder.exists()) {
      folder.mkdirs();
    }
    this.goodFile = new File(folder, "good" + extName);
    this.goodTokenLexemeFile = new File(folder, "good.token.lexeme.txt");
    this.goodTokenTypeFile = new File(folder, "good.token.type.txt");

    this.badFile = new File(folder, "bad" + extName);
    this.badTokenLexemeFile = new File(folder, "bad.token.lexeme.txt");
    this.badTokenTypeFile = new File(folder, "bad.token.type.txt");

    this.msgFile = new File(folder, "msg.txt");
    this.errorIDFile = new File(folder, "error_id.txt");
//    this.mutatorSignature = new File(folder, "mutator_signature.txt");
    this.clangErrorTemplateFile = new File(folder, "clang_error_template.txt");
    this.reductionFolder = new File(folder, "reduction");
    if (!this.reductionFolder.exists()) {
      this.reductionFolder.mkdirs();
    }
    this.reductionSeeFile = new File(this.reductionFolder, "seed" + extName);
    this.reductionMutantFile = new File(this.reductionFolder, "mutant" + extName);

    this.reducedLinedGoodFile = new File(this.reductionFolder, "good_reduced_lined" + extName);
    this.reducedLinedBadFile = new File(this.reductionFolder, "bad_reduced_lined" + extName);
    this.reducedFormattedGoodFile =
        new File(this.reductionFolder, "good_reduced_formatted" + extName);
    this.reducedFormattedBadFile =
        new File(this.reductionFolder, "bad_reduced_formatted" + extName);
  }

  public File getSeedFileForReduction() {
    return reductionSeeFile;
  }

  public File getMutantFileForReduction() {
    return reductionMutantFile;
  }

  public String readCompilerErrorId() {
    try {
      return FileUtils.readFileToString(this.errorIDFile, Charsets.UTF_8).trim();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  //  public Properties readMutatorSignature() {}

  public String readCompilerErrorMsg() {
    try {
      return FileUtils.readFileToString(this.msgFile, Charsets.UTF_8).trim();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public String getGoodFileContentWithNoWhitespaces() {
    if (this.goodFileContentWithNoWhitespaces == null) {
      this.goodFileContentWithNoWhitespaces =
          FileUtility.readFileAndDiscardWhitespaces(this.goodFile);
    }
    return this.goodFileContentWithNoWhitespaces;
  }

  public File getReducedFormattedBadFile() {
    return reducedFormattedBadFile;
  }

  public File getReducedFormattedGoodFile() {
    return reducedFormattedGoodFile;
  }

  public File getReducedLinedBadFile() {
    return reducedLinedBadFile;
  }

  public File getReducedLinedGoodFile() {
    return reducedLinedGoodFile;
  }

  private final void checkIntegrityForFolder(File folder, IntegrityReport report) {
    if (!folder.exists()) {
      report.addProblem("The folder " + folder + " does not exit. ");
    } else if (!folder.isDirectory()) {
      report.addProblem("The folder " + folder + " is not a folder.");
    } else if (folder.listFiles().length == 0) {
      report.addProblem("The folder " + folder + " is empty");
    }
  }

  private final boolean isFileContentEmpty(File file) {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      for (String line = reader.readLine(); line != null; line = reader.readLine()) {
        if (line.trim().length() > 0) {
          return false;
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return true;
  }

  private final void checkIntegrityForFile(File file, IntegrityReport report) {
    if (!file.exists()) {
      report.addProblem("The file " + file + " does not exist");
    } else if (!file.isFile()) {
      report.addProblem("The file " + file + " is not a file");
    } else if (isFileContentEmpty(file)) {
      report.addProblem("The file " + file + " is empty");
    }
  }

  public class IntegrityReport {

    private List<String> problems = new ArrayList<>();

    public void addProblem(String problem) {
      this.problems.add(problem);
    }

    public boolean hasProblems() {
      return this.problems.size() > 0;
    }

    public void logProblems() {
      if (problems.size() > 0) {
        Logger.error("The error instance {} is mal-formed", folder);
        problems.forEach(p -> Logger.error("  problem={}", p));
      }
    }
  }

  /**
   * check the integrity of this instance folder. If the returned value is empty, then it is okay.
   * Otherwise, the returned value stores the problems.
   *
   * @return
   */
  public IntegrityReport checkIntegrity() {
    final IntegrityReport problems = new IntegrityReport();

    this.checkIntegrityForFolder(this.folder, problems);
    this.checkIntegrityForFolder(this.reductionFolder, problems);

    this.checkIntegrityForFile(this.goodFile, problems);
    this.checkIntegrityForFile(this.badFile, problems);
    this.checkIntegrityForFile(this.goodTokenLexemeFile, problems);
    this.checkIntegrityForFile(this.goodTokenTypeFile, problems);
    this.checkIntegrityForFile(this.badTokenLexemeFile, problems);
    this.checkIntegrityForFile(this.badTokenTypeFile, problems);

    this.checkIntegrityForFile(this.msgFile, problems);
    this.checkIntegrityForFile(this.errorIDFile, problems);
//    this.checkIntegrityForFile(this.mutatorSignature, problems);
    this.checkIntegrityForFile(this.clangErrorTemplateFile, problems);

    this.checkIntegrityForFile(this.reductionSeeFile, problems);
    this.checkIntegrityForFile(this.reductionMutantFile, problems);

    return problems;
  }

  static boolean isGoodFile(File file) {
    final String name = file.getName();
    return name.matches("good\\.\\w+") && SourceFile.isSourceFile(name);
  }

  private static File locateGoodFile(File instanceFolder) {
    Preconditions.checkArgument(instanceFolder != null && instanceFolder.exists());
    List<File> candidate = new ArrayList<>();
    for (File file : instanceFolder.listFiles()) {
      if (isGoodFile(file)) {
        candidate.add(file);
      }
    }
    checkState(
        candidate.size() == 1,
        "cannot locate the only one 'good' file in " + "the folder {}, candidates are {}",
        instanceFolder,
        candidate);
    return candidate.get(0);
  }
}
