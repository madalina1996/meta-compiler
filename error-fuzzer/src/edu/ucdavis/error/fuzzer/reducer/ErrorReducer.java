package edu.ucdavis.error.fuzzer.reducer;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.ucdavis.error.fuzzer.compiler.AbstractCompilerTestConfig;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.core.AbstractFuzzingEngine;
import edu.ucdavis.error.fuzzer.core.Shell;
import edu.ucdavis.error.fuzzer.core.Shell.CmdOutput;
import edu.ucdavis.error.fuzzer.message.CompilerError;
import edu.ucdavis.error.fuzzer.message.ErrorParser;
import edu.ucdavis.error.fuzzer.repository.ErrorInstance;
import edu.ucdavis.error.fuzzer.token.cdt.CDTParser;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import edu.ucdavis.error.fuzzer.util.ClangFormat;
import edu.ucdavis.error.fuzzer.util.TimeoutUtil;
import org.apache.commons.io.FileUtils;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.eclipse.cdt.core.dom.ast.IASTFileLocation;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.parser.IToken;
import org.pmw.tinylog.Logger;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkState;

public class ErrorReducer {

  private static List<String> readLines(File file) {
    List<String> result = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      for (String line = reader.readLine(); line != null; line = reader.readLine()) {
        result.add(line.trim());
      }
    } catch (IOException e) {
      Logger.error("fail to open file {}", file);
      throw new RuntimeException(e);
    }
    return result;
  }

  final File workingDirectory;
  final File tempDirectory;
  final SourceFile seedFile;
  List<String> currentBest;
  final String expectedErrorMsg;
  final AbstractCompilerTestConfig compiler;
  final Edit edit;
  final ImmutableSet<String> allowedWarnings;
  final File reducedLinedGoodFile;
  final File reducedLinedBadFile;
  final File reducedFormattedGoodFile;
  final File reducedFormattedBadFile;

  private static final String flatten(List<String> content) {
    final StringBuilder builder = new StringBuilder();
    for (int i = 0, size = content.size(); i < size; ++i) {
      if (i != 0) {
        builder.append('\n');
      }
      builder.append(content.get(i));
    }
    return builder.toString();
  }

  static String toString(IASTNode node) {
    StringBuilder builder = new StringBuilder();
    try {
      IToken token = node.getSyntax();
      while (token != null) {
        builder.append(token.toString()).append(" ");
        token = token.getNext();
      }
    } catch (ExpansionOverlapsBoundaryException e) {
      e.printStackTrace();
    }

    return builder.toString();
  }

  public ErrorReducer(ErrorInstance errorInstance) {
    this(
        errorInstance.getSeedFileForReduction(),
        errorInstance.getMutantFileForReduction(),
        errorInstance.readCompilerErrorMsg(),
        errorInstance.getReducedLinedGoodFile(),
        errorInstance.getReducedLinedBadFile(),
        errorInstance.getReducedFormattedGoodFile(),
        errorInstance.getReducedFormattedBadFile());
  }

  private ErrorReducer(
      File seedFile,
      File mutantFile,
      String expectedErrorMsg,
      File reducedLinedGoodFile,
      File reducedLinedBadFile,
      File reducedFormattedGoodFile,
      File reducedFormattedBadFile) {
    seedFile = seedFile.getAbsoluteFile();
    this.seedFile = new SourceFile(seedFile);
    this.currentBest = readLines(seedFile);
    final List<String> mutant = readLines(mutantFile);

    this.edit = Edit.computeEdit(this.currentBest, mutant);
    assert (mutant.equals(this.edit.duplicateAndEdit(this.currentBest)));

    this.workingDirectory = seedFile.getParentFile();
    this.tempDirectory = new File(this.workingDirectory, "temp");
    if (!this.tempDirectory.exists()) {
      this.tempDirectory.mkdir();
    }
    this.expectedErrorMsg = normalizeErrorMsg(expectedErrorMsg);

    this.compiler = AbstractFuzzingEngine.getPrimaryCompilerConfig(this.seedFile.isCppFile());

    this.allowedWarnings = computeAllowedWarnings();

    this.reducedLinedGoodFile = reducedLinedGoodFile;
    this.reducedLinedBadFile = reducedLinedBadFile;
    this.reducedFormattedGoodFile = reducedFormattedGoodFile;
    this.reducedFormattedBadFile = reducedFormattedBadFile;
  }

  private ImmutableSet<String> computeAllowedWarnings() {
    final File tempFile = this.createTempFileFor(this.seedFile.getAbsoluteFile(), "orig", "");
    saveProgram(this.currentBest, tempFile);
    return ImmutableSet.copyOf(
        ErrorParser.parseWarnings(this.compileProgram(tempFile).getStderr()));
  }

  private File createTempFile(String name) {
    return new File(this.tempDirectory, name);
  }

  private File createTempFileFor(File orig, String infix, String suffix) {
    infix = infix == null || infix.isEmpty() ? "" : infix + "_";
    suffix = suffix == null ? "" : suffix;
    return createTempFile("temp_" + infix + orig.getName() + suffix);
  }

  private List<String> singleDeltaReductionPass(List<String> program) {
    List<String> best = new ArrayList<>(program);

    final File tempFile =
        this.createTempFileFor(this.seedFile.getAbsoluteFile(), "delta-reduction", "");
    saveProgram(best, tempFile);

    List<Integer> nonBlankIndices = computeNonBlankIndices(best);
    int granularity = nonBlankIndices.size() / 2;

    while (granularity > 0) {
      final int size = nonBlankIndices.size();
      final int colCount = granularity;
      final int rowCount = size / colCount + (size % colCount == 0 ? 0 : 1);

      for (int row = 0; row < rowCount; ++row) {
        final List<Integer> toRemove = new ArrayList<>();
        for (int col = 0; col < colCount; ++col) {
          final int i = row * colCount + col;
          if (i >= size || nonBlankIndices.get(i) == null) {
            break;
          }
          toRemove.add(nonBlankIndices.get(i));
        }
        if (toRemove.isEmpty()) {
          continue;
        }
        // List<String> testCopy = new ArrayList<>(workcopy);
        final List<String> workcopy = new ArrayList<>(best);
        for (Integer index : toRemove) {
          workcopy.set(index, "");
        }
        if (testOracle(workcopy)) {
          best.clear();
          best.addAll(workcopy);
          for (int col = 0; col < colCount; ++col) {
            final int i = row * colCount + col;
            if (i >= size) {
              break;
            }
            nonBlankIndices.set(i, null);
          }
        }
      }
      if (granularity <= 5) {
        --granularity;
      } else {
        granularity /= 2;
      }
    }
    return best;
  }

  private static String normalizeErrorMsg(String errorMessage) {
    return errorMessage.replaceAll("'[^']+'", "''");
  }

  private List<Integer> computeNonBlankIndices(List<String> best) {
    List<Integer> result = new ArrayList<>();
    for (int i = 0, size = best.size(); i < size; ++i) {
      if (best.get(i).isEmpty()) {
        continue;
      }
      result.add(i);
    }
    return result;
  }

  private CmdOutput compileProgram(File sourceFile) {
    final File objectFile = createTempFileFor(sourceFile, "", ".obj");
    final String cmd =
        TimeoutUtil.getTimeoutCmd()
            + " -s 9 120 "
            + this.compiler.getCompilerCmd()
            + sourceFile.getAbsolutePath()
            + " -o "
            + objectFile.getAbsolutePath();
    return Shell.run(cmd);
  }

  private boolean isNewWarningTriggered(CmdOutput output) {
    final List<String> warnings = ErrorParser.parseWarnings(output.getStderr());
    for (String warning : warnings) {
      if (!this.allowedWarnings.contains(warning)) {
        // this.allowedWarnings.forEach(e -> Logger.info(e));
        // Logger.info(warning);
        return true;
      }
    }
    return false;
  }

  static boolean isListEmpty(List<String> list) {
    return list.stream().filter(line -> !Strings.isNullOrEmpty(line)).limit(1).count() == 0;
  }

  public boolean testOracle(List<String> candidate) {
    if (isListEmpty(candidate)) {
      // the candidate should NOT be empty.
      return false;
    }
    // the seed should compile
    final File origFile =
        createTempFileFor(this.seedFile.getAbsoluteFile(), "oracle-test-orig", "");
    saveProgram(candidate, origFile);

    final CmdOutput compilationOfOrigFile = compileProgram(origFile);
    if (compilationOfOrigFile.getExitCode() != 0) {
      return false;
    }
    //    if (isNewWarningTriggered(compilationOfOrigFile)) {
    //      return false;
    //    }
    try {
      new CDTParser().parse(new SourceFile(origFile));
    } catch (ParsingFailureException e) {
      return false;
    }
    // generate a mutant,
    final List<String> mutant = this.edit.duplicateAndEdit(candidate);
    final File mutantFile =
        this.createTempFileFor(this.seedFile.getAbsoluteFile(), "oracle-test-mutant", "");
    saveProgram(mutant, mutantFile);
    final CmdOutput cmdOutput = compileProgram(mutantFile);
    // the mutant should not compile;
    if (cmdOutput.getExitCode() == 0) {
      return false;
    }
    // the mutant should not compile, and fail with the same fatal error
    final CompilerError compilerError = ErrorParser.parseFirstError(cmdOutput.getStderr());
    if (compilerError == null
        || !normalizeErrorMsg(compilerError.getMessage()).contains(this.expectedErrorMsg)) {
      return false;
    }
    return true;
  }

  private static int countTokens(List<String> program) {
    return (int) program.stream().filter(s -> s.trim().length() > 0).count();
  }

  public void reduce() {
    Logger.debug("start reduction");
    Logger.debug("perform sanity check...");
    this.sanityCheck(this.currentBest);
    String prevContent = "";

    int iteration = 1;
    while (true) {
      Logger.info("reduction iteration {}", iteration++);

      int prevSize = countTokens(this.currentBest);
      this.currentBest = this.singleASTReductionPass(this.currentBest);
      int currentSize = countTokens(this.currentBest);
      Logger.info(
          "AST reduction ratio: before = {}, after = {}", prevSize, countTokens(this.currentBest));

      prevSize = currentSize;
      this.currentBest = this.singleDeltaReductionPass(this.currentBest);
      currentSize = countTokens(this.currentBest);
      Logger.info("Delta reduction ratio: before = {}, after = {}", prevSize, currentSize);

      prevSize = currentSize;
      this.currentBest = this.singleTokenSeqReductionPass(this.currentBest);
      currentSize = countTokens(this.currentBest);
      Logger.info("Tokenseq reduction ratio: before = {}, after = {}", prevSize, currentSize);

      prevSize = currentSize;
      this.currentBest = this.removeRedundantParentheses(this.currentBest);
      currentSize = countTokens(this.currentBest);
      Logger.info("Parens reduction ration: before = {}, after = {}", prevSize, currentSize);

      final String currContent = flatten(this.currentBest);

      if (prevContent.equals(currContent)) {
        Logger.info("fix point reached.");
        break;
      }
      prevContent = currContent;
    }

    ExamplePair simplifiedPair =
        ExamplePair.condenseExamplesByRemovingBlankLines(this.currentBest, this.edit);

    this.outputLinedPrograms(simplifiedPair);
    this.outputFormattedPrograms(simplifiedPair);
  }

  private static class Parentheses {

    private final int openIndex;

    private int closeIndex;

    private int type;

    public Parentheses(int openIndex, int type) {
      this.openIndex = openIndex;
      this.type = type;
    }
  }

  private static final ImmutableList<String> OPEN_LIST;

  private static final ImmutableList<String> CLOSE_LIST;

  static {
    OPEN_LIST = ImmutableList.of("(", "[", "{");
    CLOSE_LIST = ImmutableList.of(")", "]", "}");
  }

  private List<Parentheses> readParentheses(List<String> program) {
    final Stack<Parentheses> stack = new Stack<>();
    final List<Parentheses> result = new ArrayList<>();
    for (int i = 0, size = program.size(); i < size; ++i) {
      final String token = program.get(i);
      if (token.length() != 1) {
        continue;
      }
      {
        final int type = OPEN_LIST.indexOf(token);
        if (type >= 0) {
          stack.push(new Parentheses(i, type));
          continue;
        }
      }
      {
        final int type = CLOSE_LIST.indexOf(token);
        if (type < 0) {
          continue;
        }
        final Parentheses top = stack.pop();
        checkState(top.type == type);
        top.closeIndex = i;
        result.add(top);
      }
    }
    checkState(stack.isEmpty());
    return result;
  }

  private List<String> removeRedundantParentheses(List<String> program) {
    final List<Parentheses> pairs = readParentheses(program);

    final List<String> best = new ArrayList<>(program);

    final File tempFile =
        this.createTempFileFor(this.seedFile.getAbsoluteFile(), "tokenseq-reduction", "");
    saveProgram(best, tempFile);

    for (Parentheses p : pairs) {
      final String open = best.get(p.openIndex);
      final String close = best.get(p.closeIndex);

      best.set(p.openIndex, "");
      best.set(p.closeIndex, "");

      if (testOracle(best)) {
        // do nothing.
      } else {
        best.set(p.openIndex, open);
        best.set(p.closeIndex, close);
      }
    }
    return best;
  }

  private List<String> singleTokenSeqReductionPass(List<String> program) {
    final List<String> best = new ArrayList<>(program);

    final File tempFile =
        this.createTempFileFor(this.seedFile.getAbsoluteFile(), "tokenseq-reduction", "");
    saveProgram(best, tempFile);

    int seqLength = 6;

    while (seqLength > 1) {
      final List<Integer> nonBlankIndices = computeNonBlankIndices(best);
      final int size = nonBlankIndices.size();
      if (size <= 2) {
        break;
      }
      if (seqLength >= size) {
        seqLength = size - 1;
      }

      for (int i = 0; i < size - seqLength + 1; ++i) {
        final List<String> workcopy = new ArrayList<>(best);
        for (int j = 0; j < seqLength; ++j) {
          workcopy.set(nonBlankIndices.get(i + j), "");
        }
        if (testOracle(workcopy)) {
          best.clear();
          best.addAll(workcopy);
          i = i + seqLength - 1;
        }
      }
      --seqLength;
    }
    return best;
  }

  static class ExamplePair {

    private final List<String> goodTokenList;

    private final List<String> badTokenList;

    ExamplePair(List<String> goodTokenList, List<String> badTokenList) {
      this.goodTokenList = goodTokenList;
      this.badTokenList = badTokenList;
    }

    private static void removeNullFromList(List<String> list) {
      int end = 0;
      final int size = list.size();
      for (int i = 0; i < size; ++i) {
        if (list.get(i) == null) {
          continue;
        }
        list.set(end++, list.get(i));
      }
      while (end++ < size) {
        list.remove(list.size() - 1);
      }
    }

    public static ExamplePair condenseExamplesByRemovingBlankLines(List<String> seed, Edit edit) {
      List<String> program = new ArrayList<>(seed);
      List<String> mutant = edit.duplicateAndEdit(program);
      for (int i = 0, size = program.size(); i < size; ++i) {
        if (program.get(i).isEmpty() && mutant.get(i).isEmpty()) {
          program.set(i, null);
          mutant.set(i, null);
        }
      }
      removeNullFromList(program);
      removeNullFromList(mutant);
      return new ExamplePair(program, mutant);
    }
  }

  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    {
      list.clear();
      list.addAll(Arrays.asList("", null, "", null, null, null));
      ExamplePair.removeNullFromList(list);
      System.out.println(list);
    }
  }

  void outputFormattedPrograms(ExamplePair simplifiedPair) {
    outputGoodFormattedSourceFile(simplifiedPair);
    outputBadFormattedSourceFile(simplifiedPair);
  }

  private static final boolean isPartOfIdentifier(int c) {
    return Character.isLetterOrDigit(c) || c == '_';
  }

  private static final boolean needWhitespaceDelimiter(
      final ArrayList<String> builder, final String current) {
    if (builder.isEmpty()) {
      return false;
    }
    if (current.isEmpty()) {
      return false;
    }
    String previous = "";
    for (int i = builder.size() - 1; i >= 0 && previous.isEmpty(); --i) {
      previous = builder.get(i);
    }
    if (previous.isEmpty()) {
      return false;
    }
    final char lastChar = previous.charAt(previous.length() - 1);
    if (Character.isWhitespace(lastChar)) {
      return false;
    }
    final char currentFirstChar = current.charAt(0);
    if (isPartOfIdentifier(lastChar) && isPartOfIdentifier(currentFirstChar)) {
      return true;
    }
    return false;
  }

  static String formatBadSourceCode(ExamplePair examplePair, File formattedGoodSourceFile) {
    final List<String> goodTokenList = examplePair.goodTokenList;
    final List<String> badTokenList = examplePair.badTokenList;
    checkState(
        goodTokenList.size() == badTokenList.size(),
        "The two lists should have the same size, %s vs %s",
        goodTokenList.size(),
        badTokenList.size());

    try {
      //      final StringBuilder builder = new StringBuilder();
      final ArrayList<String> builder = new ArrayList<>();
      final ImmutableList<IProgramSegment> segments =
          NonTokenPreservedTokenizer.tokenize(formattedGoodSourceFile);
      final int tokenCount = goodTokenList.size();
      checkState(
          segments.size() >= tokenCount, "segments=%s, goodTokens=%s", segments, goodTokenList);
      int segmentIndex = 0;
      for (int tokenIndex = 0; tokenIndex < tokenCount; ++tokenIndex) {
        final String goodToken = goodTokenList.get(tokenIndex);
        final String badToken = badTokenList.get(tokenIndex);
        if (goodToken.isEmpty()) {
          // mutation by insertion
          if (needWhitespaceDelimiter(builder, badToken)) {
            builder.add(" ");
          }
          builder.add(badToken);
          continue;
        } else {
          // no mutation, or mutation by either substitution or deletion.
          IProgramSegment segment;
          while (!((segment = segments.get(segmentIndex)) instanceof TokenProgramSegment)) {
            builder.add(segment.getLexeme());
            ++segmentIndex;
          }
          checkState(
              segment.getLexeme().equals(goodToken),
              "segment=%s, goodToken=%s,\nsegments=%s\ngoodTokenList=%s",
              segment.getLexeme(),
              goodToken,
              segments,
              goodTokenList);
          if (needWhitespaceDelimiter(builder, badToken)) {
            builder.add(" ");
          }
          builder.add(badToken);
          ++segmentIndex;
        }
      }
      for (int segmentCount = segments.size(); segmentIndex < segmentCount; ++segmentIndex) {
        final IProgramSegment segment = segments.get(segmentIndex);
        checkState(!(segment instanceof TokenProgramSegment));
        builder.add(segment.getLexeme());
      }
      return String.join("", builder);
    } catch (Exception e) {
      throw new AssertionError(e);
    }
  }

  /** output the formatted source code for bad program of the example pair. */
  private void outputBadFormattedSourceFile(ExamplePair examplePair) {
    String formatBadSourceCode = formatBadSourceCode(examplePair, reducedFormattedGoodFile);
    try {
      FileUtils.write(reducedFormattedBadFile, formatBadSourceCode, Charsets.UTF_8);
    } catch (IOException e) {
      throw new IOError(e);
    }
  }

  private void outputGoodFormattedSourceFile(ExamplePair simplifiedPair) {
    File goodFile = this.reducedFormattedGoodFile;
    this.outputLinedProgram(
        goodFile,
        simplifiedPair
            .goodTokenList
            .stream()
            .filter(line -> !Strings.isNullOrEmpty(line))
            .collect(Collectors.toList()));
    ClangFormat.formatSourceFile(goodFile);
  }

  private void outputLinedPrograms(ExamplePair simplifiedPair) {
    outputLinedProgram(this.reducedLinedGoodFile, simplifiedPair.goodTokenList);
    outputLinedProgram(this.reducedLinedBadFile, simplifiedPair.badTokenList);
  }

  private void outputLinedProgram(File file, List<String> program) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      for (String s : program) {
        writer.append(s).append("\n");
      }
    } catch (IOException e) {
      Logger.error(e);
      throw new RuntimeException(e);
    }
    Logger.warn("saved reduced program to {}", file);
  }

  private void sanityCheck(List<String> program) {
    Logger.debug("performing sanity check...");
    if (!this.testOracle(program)) {
      throw new RuntimeException(
          "The program failed at the sanity check. Seed file is "
              + this.seedFile.getAbsoluteFile());
    }
  }

  private static void saveProgram(List<String> program, File file) {
    try {
      FileUtils.writeStringToFile(file, flatten(program), Charsets.UTF_8, false);
    } catch (IOException e) {
      Logger.error(e);
      throw new RuntimeException(e);
    }
  }

  private List<String> singleASTReductionPass(List<String> program) {
    List<String> best = new ArrayList<>(program);

    final File tempFile =
        this.createTempFileFor(this.seedFile.getAbsoluteFile(), "ast-reduction", "");
    saveProgram(best, tempFile);
    try {
      IASTTranslationUnit translationUnit = new CDTParser().parse(new SourceFile(tempFile));
      Queue<IASTNode> queue = new LinkedList<>();
      queue.add(translationUnit);

      while (queue.size() > 0) {
        final IASTNode node = queue.poll();
        IASTFileLocation location = node.getFileLocation();
        if (location == null) continue;
        final int startingLine = location.getStartingLineNumber() - 1;
        final int endingLine = location.getEndingLineNumber() - 1;
        final ArrayList<String> workcopy = new ArrayList<>(best);
        for (int i = startingLine; i <= endingLine; ++i) {
          workcopy.set(i, "");
        }
        if (testOracle(workcopy)) {
          // accept this deletion. and make the changes permanent.
          best.clear();
          best.addAll(workcopy);
          continue;
        } else {
          final IASTNode[] children = node.getChildren();
          for (int i = children.length - 1; i >= 0; --i) {
            queue.add(children[i]);
          }
        }
      }
    } catch (ParsingFailureException e) {
      throw new RuntimeException(e);
    }
    return best;
  }
}
