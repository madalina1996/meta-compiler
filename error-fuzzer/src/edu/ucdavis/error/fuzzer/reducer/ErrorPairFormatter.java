package edu.ucdavis.error.fuzzer.reducer;

import com.google.common.base.Charsets;
import edu.ucdavis.error.fuzzer.repository.ErrorInstance;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/** Created by neo on 6/19/17. */
public class ErrorPairFormatter extends ErrorReducer {

  public ErrorPairFormatter(ErrorInstance errorInstance) {
    super(errorInstance);
  }

  public void format() throws IOException {
    final List<String> goodList =
        Files.readAllLines(this.reducedLinedGoodFile.toPath(), Charsets.UTF_8);
    final List<String> badList =
        Files.readAllLines(this.reducedLinedBadFile.toPath(), Charsets.UTF_8);
    final ExamplePair pair = new ExamplePair(goodList, badList);

    this.outputFormattedPrograms(pair);
  }
}
