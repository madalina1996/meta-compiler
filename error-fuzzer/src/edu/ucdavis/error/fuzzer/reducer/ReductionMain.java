package edu.ucdavis.error.fuzzer.reducer;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.ErrorInstance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/** Created by Chengnian Sun on 5/6/17. */
public class ReductionMain {

  private static class Options {
    private static final Options INSTANCE = new Options();

    @Parameter(
      names = {"--error_repo_folder"},
      description = "The folder of the error repository"
    )
    private String errorRepoFolder;

    @Parameter(names = "--help", help = true)
    private boolean help;
  }

  private static class ReductionThread extends Thread {

    private final ConcurrentLinkedQueue<ErrorInstance> queue;
    private final ConcurrentLinkedQueue<InstanceFailedToReduce> failedQueue;

    private ReductionThread(
        int id,
        ConcurrentLinkedQueue<ErrorInstance> queue,
        ConcurrentLinkedQueue<InstanceFailedToReduce> failedInstances) {
      super("ReductionThread-" + id);
      this.queue = queue;
      this.failedQueue = failedInstances;
    }

    @Override
    public void run() {
      ErrorInstance instance = null;
      while ((instance = queue.poll()) != null) {
        try {
          new ErrorReducer(instance).reduce();
        } catch (Throwable e) {
          e.printStackTrace();
          failedQueue.add(new InstanceFailedToReduce(instance, e));
        }
      }
    }
  }

  public static void main(String[] args) {
    final Options cmd = new Options();
    JCommander commander = new JCommander(cmd, args);

    if (cmd.help) {
      commander.usage();
      return;
    }
    checkNotNull(cmd.errorRepoFolder, "The error repo folder is not specified.");
    final File errorRepoFolder = new File(cmd.errorRepoFolder);
    checkArgument(
        errorRepoFolder.isDirectory(),
        "The error repository folder %s does not exit.",
        errorRepoFolder);
    final CompilerErrorRepository repo =
        CompilerErrorRepository.loadExistingErrorRepository(errorRepoFolder, Integer.MAX_VALUE);
    final ConcurrentLinkedQueue<ErrorInstance> errorInstances = repo.toErrorInstanceQueue();
    final ConcurrentLinkedQueue<InstanceFailedToReduce> failedInstances =
        new ConcurrentLinkedQueue<>();

    final List<ReductionThread> reductionThreads =
        IntStream.range(1, 8)
            .mapToObj(i -> new ReductionThread(i, errorInstances, failedInstances))
            .collect(Collectors.toList());
    reductionThreads.forEach(Thread::start);

    reductionThreads.forEach(
        thread -> {
          try {
            thread.join();
          } catch (InterruptedException e) {
            throw new AssertionError(e);
          }
        });

    if (!failedInstances.isEmpty()) {
      int i = 1;
      try (PrintWriter writer =
          new PrintWriter(new FileWriter("failed_to_reduced_error_instances.txt"))) {
        for (InstanceFailedToReduce instance = failedInstances.poll();
            instance != null;
            instance = failedInstances.poll()) {
          writer
              .append("======================= ")
              .append("Instance ")
              .append(Integer.toString(i++))
              .append(" =======================");
          writer.append(instance.errorInstance.getFolder().getAbsolutePath());
          writer.append('\n');

          instance.exception.printStackTrace(writer);
          writer.append("\n");
          writer.append("\n");
          writer.append("\n");
        }
      } catch (IOException e) {
        throw new AssertionError("Fail to dump the failed_to_reduce instances ", e);
      }
    }
  }

  private static class InstanceFailedToReduce {
    private final ErrorInstance errorInstance;
    private final Throwable exception;

    public InstanceFailedToReduce(ErrorInstance errorInstance, Throwable exception) {
      this.errorInstance = errorInstance;
      this.exception = exception;
    }
  }
}
