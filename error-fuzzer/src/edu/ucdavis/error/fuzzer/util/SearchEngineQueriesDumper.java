package edu.ucdavis.error.fuzzer.util;

import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.message.ClangDiagnosticsMsgTemplate;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class SearchEngineQueriesDumper {

  private final static class EscapedCharMapping {

    private final String escaped;

    private final String original;

    public EscapedCharMapping(String escaped, char original) {
      this.escaped = escaped;
      this.original = String.valueOf(original);
    }
  }

  private final static ImmutableList<EscapedCharMapping> REGEXP_META_CHARS;

  static {
    ImmutableList.Builder<EscapedCharMapping> builder = ImmutableList.builder();
    final String metaChars = "<([^-=!])?*+.>{}";
    for (int i = 0, length = metaChars.length(); i < length; ++i) {
      final char c = metaChars.charAt(i);
      builder.add(new EscapedCharMapping("\\\\\\" + c, c));
    }
    REGEXP_META_CHARS = builder.build();
  }

  public static List<String> constructGoogleQueryFromPattern(String regexp) {
    final String[] segments = regexp.split("\\.(\\+|\\?|\\*)");
    List<String> result = new ArrayList<>();
    for (int i = 0; i < segments.length; i++) {
      String s = segments[i];
      s = s.trim();
      if (s.isEmpty()) {
        continue;
      }
      for (EscapedCharMapping mapping : REGEXP_META_CHARS) {
        s = s.replaceAll(mapping.escaped, mapping.original);
      }
      result.add(s);
    }
    return result;
  }

  private static void constructQueries(PrintStream stream) {
    List<ClangDiagnosticsMsgTemplate> allTemplates = ClangDiagnosticsMsgTemplate.getAllTemplates();

    for (ClangDiagnosticsMsgTemplate template : allTemplates) {
      Logger.info("Processing the template {}", template);
      stream.println("Query for error " + template.getId());

      final String pattern = template.getPattern().pattern();
      stream.println("original regexp: " + pattern);
      stream.println("Google query segments: ");
      for (String segment : constructGoogleQueryFromPattern(pattern)) {
        stream.println("    " + segment);
      }
      stream.println();
      stream.println();
    }
  }

  public static void main(String[] args) {
    final File file = new File("google-queries.txt");
    try (PrintStream stream = new PrintStream(new FileOutputStream(file))) {
      constructQueries(stream);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    Logger.info("Saved result to {}", file);
  }
}
