package edu.ucdavis.error.fuzzer.util;

public class TimeoutUtil {

  private static final boolean IS_MAC_OS;

  private static final String CMD_TIMEOUT;

  static {
    final String osName = System.getProperty("os.name", "unknown")
        .toLowerCase();
    IS_MAC_OS = osName.startsWith("mac") || osName.startsWith("darwin");
    CMD_TIMEOUT = IS_MAC_OS ? "gtimeout" : "timeout";
  }

  public static String getTimeoutCmd() {
    return CMD_TIMEOUT;
  }

}
