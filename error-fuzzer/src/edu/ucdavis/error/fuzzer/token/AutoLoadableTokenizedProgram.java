package edu.ucdavis.error.fuzzer.token;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by Chengnian Sun on 4/2/17.
 *
 * <p>This class is a wrapper of TokenizedProgram. It uses a weak reference to the tokenized
 * program. When memory is limited, the program will be released. However, when a client requests
 * this program, the program will be reloaded from the file system.
 */
public class AutoLoadableTokenizedProgram {

  private final File tokenizedProgramFile;

  private WeakReference<TokenizedProgram> reference;

  public AutoLoadableTokenizedProgram(File tokenizedProgramFile) {
    this.tokenizedProgramFile = tokenizedProgramFile;
    this.reference = new WeakReference<TokenizedProgram>(null);
  }

  public TokenizedProgram get() {
    TokenizedProgram result = reference.get();
    if (result == null) {
      try {
        result = TokenizedProgram.readTokenizedProgram(this.tokenizedProgramFile);
      } catch (IOException e) {
        throw new AssertionError(e);
      }
      this.reference = new WeakReference<TokenizedProgram>(result);
    }
    return result;
  }
}
