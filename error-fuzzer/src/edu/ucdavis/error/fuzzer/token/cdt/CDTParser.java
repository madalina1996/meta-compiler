package edu.ucdavis.error.fuzzer.token.cdt;

import java.io.File;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.dom.ast.gnu.c.GCCLanguage;
import org.eclipse.cdt.core.dom.ast.gnu.cpp.GPPLanguage;
import org.eclipse.cdt.core.index.IIndex;
import org.eclipse.cdt.core.model.ILanguage;
import org.eclipse.cdt.core.parser.ExtendedScannerInfo;
import org.eclipse.cdt.core.parser.FileContent;
import org.eclipse.cdt.core.parser.IParserLogService;
import org.eclipse.cdt.core.parser.IScannerInfo;
import org.eclipse.cdt.core.parser.IncludeFileContentProvider;
import org.eclipse.cdt.core.parser.NullLogService;
import org.eclipse.cdt.core.parser.util.ASTPrinter;
import org.eclipse.cdt.internal.core.dom.parser.c.CVisitor.CollectProblemsAction;
import org.eclipse.core.runtime.CoreException;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;

public class CDTParser {

  private final static IParserLogService LOG_SERVICE = new NullLogService();

  private final static IScannerInfo SCANNER_INFO;

  static {
    final Map<String, String> definedSymbols = ImmutableMap.of();
    final String[] includePaths = new String[0];
    SCANNER_INFO = new ExtendedScannerInfo(definedSymbols, includePaths);
  }

  public static void main(final String[] args) throws ParsingFailureException {
    final File cppFile = new File("examples/t1.c");
    final IASTTranslationUnit unit = new CDTParser()
        .parse(new SourceFile(cppFile));
    // System.out.println(unit);
    ASTPrinter.print(unit);
  }

  // 0 or ILanguage.OPTION_IS_SOURCE_UNIT
  private final int parsingOption;

  public CDTParser(/* boolean isSourceUnit */) {
    super();
    this.parsingOption = ILanguage.OPTION_IS_SOURCE_UNIT
        | ILanguage.OPTION_NO_IMAGE_LOCATIONS;
  }

  protected IIndex createIndex() {
    return null;
  }

  protected IParserLogService createLogService() {
    // return new StdoutLogService();
    return CDTParser.LOG_SERVICE;
  }

  protected IScannerInfo createScannerInfo() {
    // Map<String, String> definedSymbols = ImmutableMap.of();
    // String[] includePaths = CompilerConfig.INCLUDE_PATH
    // .toArray(new String[CompilerConfig.INCLUDE_PATH.size()]);
    // return new ExtendedScannerInfo(definedSymbols, includePaths);
    return CDTParser.SCANNER_INFO;
  }

  public IASTTranslationUnit parse(final SourceFile file)
      throws ParsingFailureException {
    Preconditions.checkArgument(file.getAbsoluteFile().exists(),
        "The source file %s does not exist.", file);

    final FileContent content = FileContent.createForExternalFileLocation(
        file.getAbsoluteFile().getAbsolutePath());

    final IScannerInfo scanInfo = createScannerInfo();

    final IncludeFileContentProvider fileCreator = IncludeFileContentProvider
        .getEmptyFilesProvider();
    final IIndex index = createIndex();
    final IParserLogService log = createLogService();
    final int option = this.parsingOption;
    IASTTranslationUnit unit;
    try {
      unit = (file.isCppFile() ? GPPLanguage.getDefault()
          : GCCLanguage.getDefault()).getASTTranslationUnit(content, scanInfo,
              fileCreator, index, option, log);
    } catch (Throwable e) {
      throw new ParsingFailureException(e);
    }

    final CollectProblemsAction problemsAction = new CollectProblemsAction();
    unit.accept(problemsAction);
    if (problemsAction.getProblems().length != 0) {
      throw new ParsingFailureException(problemsAction.getProblems());
    }
    return unit;
  }

}
