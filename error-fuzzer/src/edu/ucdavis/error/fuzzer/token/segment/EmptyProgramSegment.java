package edu.ucdavis.error.fuzzer.token.segment;

/** Created by Chengnian Sun on 4/27/17. */
public class EmptyProgramSegment extends AbstractProgramSegment {

  public static final EmptyProgramSegment EMPTY_TOKEN = new EmptyProgramSegment();

  private EmptyProgramSegment() {
    super("");
  }
}
