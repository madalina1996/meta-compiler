package edu.ucdavis.error.fuzzer.token.segment;

/** Non-tokens, such as comments, white spaces. */
public class NonTokenProgramSegment extends AbstractProgramSegment {

  public NonTokenProgramSegment(String lexeme) {
    super(lexeme);
  }
}
