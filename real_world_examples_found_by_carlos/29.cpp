#include <stdlib.h>
#include<stdio.h>
void x_function(void *d)
{
    printf("the integer value of d is %d \n " , *(int *)d );
    printf("the string  value of d is %s \n " , (char *)d );
    printf("the character value of d is %c \n " , *(char *)d );
    printf("the double value of d is %lf \n " , *(double *)d );
	// FIX: delete 10 below
    return 10;
}

int main()
{
    void *x = (void*)new int(10);
    x_function(x);
    return 0;
}