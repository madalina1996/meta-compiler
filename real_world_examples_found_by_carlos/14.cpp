/////HERE'S MY VERTEX CLASS///
#include <vector>
#include <climits>
#include <fstream>
#include<iostream>
using namespace std;
class Edge{
    public:
    void setWeight(int w){}
    void setAdjVertex(int w){}
    int getAdjVertex(){return 0;}
     int getWeight(){return 0;}
};
class Vertex
{
    private:
        int vertexNum; //number of the vertex for identification purposes
        int degree;
        bool known;
        vector<Edge> adjacentVertices; //vector of vertices that are adjacent to the vertex we are currently looking at
        int dv; //distance 
        int pv; //previous vertex
        Vertex *vertex;
    public:
        Vertex()
        {
            dv = INT_MAX;
            known = false;
        }

        void setKnown(bool Known)
        {
            known = Known;
        }

        bool getKnown()
        {
            return known;
        }

        void setVertexNum(int VertexNum)
        {
            vertexNum = VertexNum;
        }

        void setDegree(int Degree)
        {
            degree = Degree;
        }

        vector<Edge> & getAdjacentVertices()
        {
            return adjacentVertices;
        }

        int getVertexNum()
        {
            return vertexNum;
        }

        int getDegree()
        {
            return degree;
        }

        int getDV() const 
        {
            return dv;
        }

        void setAdjacentVertex(int AdjacentVertex, int Weight)
        {
            Edge newEdge;
            newEdge.setWeight(Weight);
            newEdge.setAdjVertex(AdjacentVertex);
            adjacentVertices.push_back(newEdge);
        }

        friend ostream & operator <<(ostream & outstream, Vertex & vertex)
        {
            outstream << vertex.vertexNum << endl;
            outstream << vertex.degree << endl;
            outstream << vertex.known << endl;
            vector<Edge> E = vertex.getAdjacentVertices();
            for(int x=0;x<E.size();x++)
            {
                outstream << E[x].getAdjVertex() << endl;
                outstream << E[x].getWeight() << endl;
            }
            return outstream;
        }

        friend bool operator < (const Vertex & v1, const Vertex & v2);

        void printVertex()
        {

        }


};
int main(){
int numVertices = 10;
int numEdges = 10;
vector<Vertex*> vertices (numVertices + 1);

for(int i=1;i<=numVertices;i++)
{   
   
    cout << "At vertex " << i << " the number of edges is " << numEdges << endl;
    vertices[i] = new Vertex();
    //Vertex newVertex;

    //Using the i counter variable in the outer for loop to identify
    //the what vertex what are currently looking at in order to read in the correct adjacent vertex and weight
    vertices[i] -> setVertexNum(i);
    //newVertex.setVertexNum(i);

}

vector<Vertex*> sortedVertices = vertices;

for(int i=0;i<vertices.size();i++)
{
    cout << "V" << i << ":  ";
    cout << endl;
	// FIX: delete * below
    for(int j=0;j<*vertices[i] -> getAdjacentVertices().size();j++)
    //for(int j=0; j < vertices[i] -> getAdjacentVertices().size(); j++)
    {
        cout << "V" << vertices[i] -> getAdjacentVertices()[j].getAdjVertex() << " " << vertices[i] -> getAdjacentVertices()[j].getWeight() << endl;
    }
}

}

